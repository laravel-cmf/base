@extends(CMFPackageName().'::base')
@section('content')
    <div class="row" id="index-controls">
        <div class="col-lg-10 col-lg-offset-1">
            @if($resource->can('create'))
                @if($resource->getDescendants('create'))
                    <dropdown text="Create" type="primary" class="pull-right">
                        <li> <a class="" href="{{$resource->getCreateLink()}}">Create
                                New {{$resource->singular()}}</a></li>
                        <li class="divider"></li>
                        @foreach($resource->getDescendants('create') as $descendantResource)
                            <li><a href="{{$descendantResource->getCreateLink()}}">Create
                                    New {{$descendantResource   ->singular()}}</a></li>
                        @endforeach
                    </dropdown>
                @else
                    <a class="pull-right btn btn-primary" href="{{$resource->getCreateLink()}}">Create
                        New {{$resource->singular()}}</a>
                @endif
            @endif
        </div>
    </div>
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th>

            </th>
            @foreach($resource->list_fields() as $listField)
                <th>{{ucfirst(str_replace("_", " ", $listField))}}</th>
            @endforeach
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $item)
            <tr>
                <td>
                    <i class="fa {{$item->icon()}}"></i>
                </td>
                @foreach($item->getListFields() as $key=>$resourceField)
                    <td>{!! $resourceField->display() !!}</td>
                @endforeach
                <td>
                    @if($item->can('edit'))
                        <a href="{{$item->getEditLink()}}" class="btn btn-warning">Edit</a>
                    @endif
                    @if($item->can('view'))
                        <a href="{{$item->getViewLink()}}" class="btn btn-primary">View</a>
                    @endif
                    @if($item->can('delete'))
                        <a href="{{$item->getDeleteLink()}}"
                           onclick="return confirm('Are you sure you want to delete this item?');"
                           class="btn btn-danger">Delete</a>
                    @endif

                    @if($item->list_actions())
                        @foreach($item->list_actions() as $action => $settings)
                            <a href="{{$item->getActionLink($action)}}"
                               class="btn btn-xs btn-default">{{$settings['text'] or $action}}</a>
                        @endforeach
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection