@if($form)
@push('cmf_assets')
    <script type="text/javascript">
        //Create a new Resource App allowing us dynamic behavior in the form
        CMFApp.Resource('#resource');
    </script>
@endpush
<div class="row">
    <div class="col-lg-12">
        <div id="resource" :resource-data="{{$resource->getJsonData() }}" style="display:none;" v-show="true">
            <cmf-form class="form-horizontal"
                      id="{{$form->getFormId()}}" method="{{$form->getMethod()}}"
                      enctype="{{$form->getEnctype()}}"
                      action="{{$form->getAction()}}" :model.sync="model">
                {{ csrf_field() }}
                <tabs>
                    @foreach($form->getTabs() as $tab)
                        <tab header="{{$tab->getTitle()}}">
                            <h1>{{$tab->getTitle()}}</h1>
                            @foreach($tab->getGroups() as $group)
                                <fieldset>
                                    <legend>{{$group->getTitle()}}</legend>
                                    @foreach($group->getFields() as $field)
                                        {!! $field->render() !!}
                                    @endforeach
                                </fieldset>
                            @endforeach
                        </tab>
                    @endforeach
                </tabs>
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="reset" class="btn btn-default">Reset</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </cmf-form>
        </div>
    </div>
</div>
@endif