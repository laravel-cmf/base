<div class="row">
    <div class="col-lg-2">
        <h4>{{$field->getTitle()}}</h4>
    </div>
</div>
@foreach($fields as $inlineField)
    {!! $inlineField->render() !!}
@endforeach