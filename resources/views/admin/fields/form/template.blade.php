<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <auto-update-field name="{{$field->getFieldName()}}" :value.sync="model.{{$resourceField->getFieldKey()}}" :template-value="{{$resourceField->getSetting('template', '')}}" :settings.sync="model.settings.{{$resourceField->getFieldKey()}}"></auto-update-field>
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
    </div>
</div>