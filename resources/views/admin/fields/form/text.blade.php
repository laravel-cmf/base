<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">

    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <input type="text" class="form-control" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}"
               placeholder="{{$resourceField->getSetting('placeholder')}}"
               v-model="model.{{$resourceField->getFieldKey()}}"
               value="{{$resourceField->value()}}">
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>