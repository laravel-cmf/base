<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="select" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <select class="form-control" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}{{$resourceField->getSetting('multiple') ? '[]' : ''}}" {{$resourceField->getSetting('multiple') ? "multiple" : ""}}>
            @if($resourceField->getSetting('nullable'))
                <option value="">Select an option</option>
            @endif
            @foreach($resourceField->getOptions() as $key => $option)
                <option value="{{$option['value']}}" {{$option['selected'] ? 'selected' : ''}}>{{$option['label']}}</option>
            @endforeach
        </select>
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>