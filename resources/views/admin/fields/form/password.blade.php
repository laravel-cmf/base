<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <input type="password" class="form-control" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}[value]" placeholder="Enter Password">
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
    </div>
    <label for="{{$field->getFieldId()}}_confirm" class="col-lg-2 control-label">{{$field->getFieldTitle()}} Confirmation</label>
    <div class="col-lg-10">
        <input type="password" class="form-control" id="{{$field->getFieldId()}}_confirm" name="{{$field->getFieldName()}}[confirm]" placeholder="Enter Confirmation">
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>