<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        @foreach($field->options as $key => $option)
        <div class="radio">
            <label>
                <input type="radio" name="{{$field->getFieldName()}}" id="{{$field->getFieldId().$key}}" value="{{$option['value']}}" {{$resourceField->value() == $option['value'] ? 'checked' : ''}} >
                {{$option['label']}}
            </label>
        </div>
        @endforeach
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>