<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        @if($resourceField->value())
            <div class="row">
                <div class="col-lg-12">
                    <div class="checkbox" style="margin:0 auto 20px; text-align:center;max-width: 50%;">
                        <img src="{{cmf_file_url($resourceField->value()->src)}}" alt="" style="max-width:100%"/>
                        <p>{{$resourceField->value()->src_url}}</p>
                        <label>
                            <input type="checkbox" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}[keep]" checked>  Keep current image
                        </label>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <input type="file" class="form-control" id="{{$field->getFieldId()}}[file]" name="{{$field->getFieldName()}}[file]">

                @include(CMFTemplate('admin.fields.form.shared.help-block'))
                @if(!$resourceField->valid())
                    @foreach($resourceField->getErrors() as $error)
                        <p class="alert alert-danger">
                            {{$error}}
                        </p>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>