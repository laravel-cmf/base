@push('cmf_assets')
    <script type="text/javascript">
        $(document).ready(function(){
            $(function () {
                $('.datetimepicker').datetimepicker({format:'Y-m-d H:i:s', step:15});
            });
        });
    </script>
@endpush

<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <input type="text" class="form-control datetimepicker" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}" value="{{$resourceField->value()}}" placeholder="{{$resourceField->getSetting('placeholder')}}">
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
            <p class="alert alert-danger">
                {{$error}}
            </p>
            @endforeach
        @endif
    </div>
</div>