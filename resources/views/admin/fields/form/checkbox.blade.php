<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
        <div class="checkbox">
            <input type="hidden" id="{{$field->getFieldId()}}_hidden" name="{{$field->getFieldName()}}" value="0">
            <label>
                <input type="checkbox" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}" {{$resourceField->value() ? 'checked' : ''}}> {{$field->getFieldTitle()}}
            </label>
            @include(CMFTemplate('admin.fields.form.shared.help-block'))
        </div>
    </div>
</div>