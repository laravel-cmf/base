@if($resourceField->getSetting('description'))
    <span class="help-block">{{$resourceField->getSetting('description')}}</span>
@endif