@push('cmf_assets')
    <script src="{{cmf_asset('vendor/ckeditor/ckeditor.js')}}"></script>
@endpush
<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        <textarea class="form-control ckeditor" rows="3" id="{{$field->getFieldId()}}" name="{{$field->getFieldName()}}" placeholder="{{$resourceField->getSetting('placeholder')}}">{{$resourceField->value()}}</textarea>
        @include(CMFTemplate('admin.fields.form.shared.help-block'))
        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
</div>