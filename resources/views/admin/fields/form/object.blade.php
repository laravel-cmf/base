<div class="form-group {{!$resourceField->valid() ? 'has-error' : ''}}">
    @if(!$resourceField->getSetting('multiple'))
    <label for="{{$field->getFieldId()}}" class="col-lg-2 control-label">{{$field->getFieldTitle()}}</label>
    <div class="col-lg-10">
        @foreach($resourceField->getObjectFields() as $fieldKey => $objectField)
            <label for="{{$field->getFieldId()}}-{{$fieldKey}}">{{$objectField['title']}}</label>
            @if($objectField['type'] === 'textarea')
                <textarea class="form-control" rows="3" id="" name="{{$field->getFieldName()}}[{{$fieldKey}}]" v-model="model.{{$resourceField->getFieldKey()}}.{{$fieldKey}}">{{$objectField['value']}}</textarea>
            @elseif($objectField['type'] === 'template')
                <auto-update-field name="{{$field->getFieldName()}}[{{$fieldKey}}]" :value.sync="model.{{$resourceField->getFieldKey()}}.{{$fieldKey}}.value" :template-value="{{$objectField['template']}}" :settings.sync="model.settings.{{$resourceField->getFieldKey()}}.{{$fieldKey}}"></auto-update-field>
            @else
                <input type="text" class="form-control" id="{{$field->getFieldId()}}-{{$fieldKey}}" name="{{$field->getFieldName()}}[{{$fieldKey}}]"
                   v-model="model.{{$resourceField->getFieldKey()}}.{{$fieldKey}}"
                   value="{{$objectField['value']}}">
            @endif
        @endforeach
        @include(CMFTemplate('admin.fields.form.shared.help-block'))

        @if(!$resourceField->valid())
            @foreach($resourceField->getErrors() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
            @endforeach
        @endif
    </div>
    @else
        <div class="col-lg-2">
            <h4>{{ucfirst($field->getFieldTitle())}}</h4>
            @include(CMFTemplate('admin.fields.form.shared.help-block'))
        </div>
        <div class="col-lg-10">
            <inline-object-field name="{{$field->getFieldName()}}" multi="true" :fields="{{json_encode($resourceField->getFieldConfig()) }}" :model.sync="model.{{$resourceField->getFieldKey()}}" limit="{{$resourceField->getSetting('limit')}}">

            </inline-object-field>
            {{--<table class="table table-bordered">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--@foreach($resourceField->getObjectFields() as $fieldKey => $objectField)--}}
                        {{--<th>{{$objectField['title']}}</th>--}}
                    {{--@endforeach--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($resourceField->value() as $object)--}}
                    {{--<tr>--}}
                        {{--@foreach($resourceField->getOjectFields() as $fieldKey => $objectField)--}}

                        {{--@endforeach--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                {{--</tbody>--}}
            </table>
        </div>
    @endif
</div>