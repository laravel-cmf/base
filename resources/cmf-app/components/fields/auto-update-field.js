var merge = require('merge');
module.exports = function () {
    return {
        template: require('./templates/auto-update-field.html'),
        props: {
            prefix: {
                type: String,
                required: false,
                default: false
            },
            settings: {
                type: Object,
                required: false,
                coerce: function (val) {
                    var defaults = {
                        'auto_generate': true,
                        'keep_updated': false
                    };
                    val = val || defaults;
                    for(var key in val) {
                        val[key] = val[key] > 0;
                    }
                    return val;
                    //merge.recursive(defaults, val || {});
                },
                twoWay: true
            },
            //The name of the input field
            name: {},
            //The two-way synced value of the field
            value: {
                required: true,
                twoWay: true
            },
            //The value to use as the template value if auto generate is on.
            templateValue: {
                required: true,
                default: ''
            }
        },
        created: function () {
            console.log(this.value);
            this.hasInitialValue = !!this.value;
            console.log(this, this.templateValue);
        },
        computed: {
            shouldUpdate: function () {
                return this.autoGenerate && (!this.hasInitialValue || this.keepUpdated);
            },
            autoGenerate: function () {
                return !!this.getSetting('auto_generate');
            },
            keepUpdated: function () {
                return !!this.getSetting('keep_updated');
            }
        },
        methods: {
            getSetting: function (key) {
                return key in this.settings ? this.settings[key] : null;
            }
        }
    }
};
