require("./css/flex.css");
var merge = require('merge');

Array.prototype.move = function (from, to) {
    this.splice(to, 0, this.splice(from, 1)[0]);
};
module.exports = function () {
    return {
        template: require('./templates/inline-object.html'),
        props: {
            name: {
                type: String,
                required: true
            },
            multi: {
                default: false
            },
            fields: {
                type: Object,
                required: true
            },
            model: {
                type: Array,
                twoWay: true,
                required: false
            },
            limit: {
                coerce: function (val) {
                    return val * 1;
                },
                default: 0
            }
        },
        data: function(){
            return {
                items: []
            };
        },
        created: function () {
            var items = [];
            var fields = this.fields;
            if (this.model.length > 0) {
                this.model.forEach(function (object) {
                    var item = merge(true, fields);
                    for (var field in item) {
                        if (field in object) {
                            item[field].value = object[field].value;
                        }
                    }
                    items.push(item);
                });
            }
            console.log(items);
            this.items = items;
        },
        watch: {
            items: function(items, oldVal) {
                this.model = items;
                console.log('watch', oldVal, items);
                // this.model = items.map(function(item){
                //     console.log(item);
                //     return '';
                // });
            }
        },
        computed: {},
        methods: {
            addItem: function () {
                if (this.limit > 0 && this.limit <= this.items.length) return false;
                var item = merge(true, this.fields);
                this.items.push(item);
            },
            removeItem: function (key) {
                if (window.confirm("Do you really want to remove?")) {
                    if (key > -1 && this.items[key]) {
                        this.items.splice(key, 1);
                    }
                }
            },
            moveUp: function (key) {
                console.log(this.items);
                if (key < 1) return false;
                this.items.move(key, key - 1);
            },
            moveDown: function (key) {
                if (key >= (this.items.length - 1)) return false;
                this.items.move(key, key + 1);
            }
        }
    }
};
