var select2 = require('select2');
module.exports = function() {
    return {
        template: require('./templates/field.html'),
        mixins: require('./mixins'),
        computed: {
            partial: function() {
                return 'input';
            }
        },
        methods: {
        }
    }
};
