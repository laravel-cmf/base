
module.exports = [
    require('./props'),
    require('./data'),
    require('./methods')
];