module.exports = function() {
    return {
        template: require('./templates/field.html'),
        /**
         * name,
         * value,
         * label,
         * hideLabel,
         * disabled
         */
        mixins: require('./mixins'),
        computed: {
            partial: function() {
                return 'input';
            }
        },
        methods: {
        }
    }
};
