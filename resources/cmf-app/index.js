window.Vue = require('vue');
window.$ = window.jQuery = require('jquery');
var VueStrap = require('vue-strap');
var CMFForm = require('./form');
var fields = require('./components/fields');
var select2 = require('select2');
require('select2/dist/css/select2.css');
require('select2-bootstrap-theme/dist/select2-bootstrap.css');
require('jquery-datetimepicker/build/jquery.datetimepicker.full');
require('jquery-datetimepicker/build/jquery.datetimepicker.min.css');

window.CMFApp = window.CMFApp || {};

CMFApp.Resource = function (el) {
    return new Vue({
        el: el,
        data: function(){
            return this.resourceData;
        },
        components: {
            cmfForm: CMFForm
        },
        props: {
            resourceData: {}
        },
        created: function () {
        }
    });
};

//Register VueStrap Components globally.
Vue.component('tabs', VueStrap.tabset);
Vue.component('tabGroup', VueStrap.tabGroup);
Vue.component('tab', VueStrap.tab);
Vue.component('formGroup', VueStrap.formGroup);
Vue.component('buttonGroup', VueStrap.buttonGroup);
Vue.component('dropdown', VueStrap.dropdown);

Vue.filter('slug', function (value) {
    if(!value) return value;
    return slugify(value.toString())
});

$(document).ready(function(){
    $('select').select2(
        {
            theme: "bootstrap"
        }
    );

    if($('#index-controls').length){
        var indexControls = new Vue({
            el: '#index-controls'
        });
    }
});

function slugify(text)
{
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
}




// Vue.directive('example', {
//     params: ['a'],
//     bind: function () {
//         var formData = JSON.parse(this.params.a);
//         if (formData) {
//             // this.vm.data = formData;
//         }
//         console.log(this.vm);
//     // }

// Vue.use(VueFormular, {
//     showClientErrorsInStatusBar: true,
//     sendOnlyDirtyFields: true,
//     layout: 'form-horizontal',
//     labelWidth: 2
// });
// var cmfResource = Vue.extend({
//     template: "<div v-el:form>{{model.title}}<slot></slot></div>",
//     props: {
//         resourceData: {
//             type: Object
//         }
//     },
//     components: {
//     },
//     data: function(){
//         this.resourceData.test = 'boo';
//         return this.resourceData;
//     },
//     created: function () {
//         console.log(this);
//     }
// });
// });
// window.CMFApp = new Vue({
//     el: '#CMF-App',
//     data: {},
//     components: {
//
//         cmfResource: cmfResource,
//         cmfForm: CMFForm
//     },
//     directives: {}
// });

// var test = new Vue({
//     el: '#resource',
//     data: function(){
//         return {
//             test: 'sickssss',
//             model: {
//                 title: 'tt'
//             }
//         }
//     },
//     components: {
//         tabs: VueStrap.tabset,
//         tabGroup: VueStrap.tabGroup,
//         tab: VueStrap.tab,
//         formGroup: VueStrap.formGroup,
//         cmfResource: cmfResource,
//         cmfForm: CMFForm
//     }
// });
