var merge = require('merge');
// var cmfForm = function (el) {
    var formConfig = {
        template: require('./templates/form.html'),
        props: {
            action: {
                type: String
            },
            method: {
                type: String,
                required: false,
                default: 'POST'
            },
            triggers: {
                type: Object,
                required: false,
                default: function () {
                    return {}
                }
            },
            options: {
                type: Object,
                required: false,
                default: function () {
                    return {}
                }
            },
            model: {
                type: Object
            }
        },
        created: function () {
            //Called synchronously after the instance is created.
            // this.formData = JSON.parse(this.formData);        console.log(this.formData);
            console.log(this.model);
        },
        data: function () {
            //Return the data object
            var data = {
                layout: 'form-horizontal'
            };
            // data = merge.recursive(data, this.formData);
            return data;
            // return {
            //     isForm: true,
            //     fields:[],
            //     additionalValues:[],
            //     errors:[],
            //     serverErrors:[],
            //     relatedFields:{},
            //     triggeredFields:{},
            //     status:'danger',
            //     statusbarMessage:'',
            //     sending:false
            // }
        },
        computed: {
            // labelClass:require('./lib/computed/label-class'),
            // fieldClass:require('./lib/computed/field-class'),
            // hasErrors: require('./lib/computed/has-errors'),
            // pristine: function() {
            //     return this.fields.length==0;
            // }
        },
        methods: {
            submit: require('./methods/submit')
            // formData:require('./lib/methods/form-data'),
            // getField:require('./lib/methods/get-field'),
            // showAllErrors:require('./lib/methods/show-all-errors'),
            // reinitForm:require('./lib/methods/reinit-form'),
            // registerInterfieldsRules: require('./lib/methods/register-interfields-rules'),
            // registerTriggers: require('./lib/methods/register-triggers')
        }

    };
    // Vue.component('cmf-form',formConfig);
    // return new Vue(formConfig);
// };

module.exports =  Vue.extend(formConfig);