var ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
    entry: './resources/cmf-app/index.js',
    output: {
        path: './public/',
        filename: 'js/cmf.app.js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: "html-loader" },
            { test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader" )}
        ]
    },
    // Use the plugin to specify the resulting filename (and add needed behavior to the compiler)
    plugins: [
        new ExtractTextPlugin("css/vendor.css")
    ]
};