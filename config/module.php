<?php

use LaravelCMF\Base\Models\Eloquent\Administrator;
use LaravelCMF\Base\Models\Eloquent\Role;
use LaravelCMF\Base\Models\Eloquent\Setting;
use LaravelCMF\Base\Resources\Fields;

return [
    'models' => [
        Administrator::class,
        Role::class,
        Setting::class
    ],
    'resource-fields' => [
        'identifier' => Fields\Scalar\Identifier::class,
        'text' => Fields\Scalar\Text::class,
        'textarea' => Fields\Scalar\Textarea::class,
        'richtext' => Fields\Scalar\Richtext::class,
        'radio' => Fields\Scalar\Radio::class,
        'select' => Fields\Scalar\Select::class,
        'checkbox' => Fields\Scalar\Checkbox::class,
        'password' => Fields\Scalar\Password::class,

        //Objects
        'datetime' => Fields\Objects\DateTime::class,
        'image' => Fields\Objects\Image::class,
        'settings' => Fields\Objects\Settings::class,
        'object' => Fields\Objects\ObjectField::class,

        //Relation
        'HasOne' => Fields\Relation\HasOne::class,
        'BelongsTo' => Fields\Relation\BelongsTo::class,
        'OneToMany' => Fields\Relation\OneToMany::class,
        'ManyToOne' => Fields\Relation\ManyToOne::class,
        'ManyToMany' => Fields\Relation\ManyToMany::class,
        'RootNode' => Fields\Relation\RootNode::class,
        'ParentNode' => Fields\Relation\ParentNode::class,

        //Computed
        'concat' => Fields\Computed\Concat::class,
        'template' => Fields\Computed\Template::class,
    ]
];