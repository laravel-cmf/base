
LaravelCMF\Base\Providers\CMFProvider::class,

'CMF' =>  LaravelCMF\Base\Support\Facades\CMF::class,

php artisan vendor:publish --force

php artisan migrate


## Forgotten Password

- Set a from name/address
- Configure Mail delivery method
- Forgotten password will now work


# Polymorphic / Multi table inheritance

Use :

    public static $_resourceExtends = Page::class;
    
    public static $_baseClass = Page::class;
    
    
    