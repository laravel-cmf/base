<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Resources\Contracts\CMFResourceField;
use LaravelCMF\Base\Resources\Fields\Objects\AbstractObjectField;

class InlineObject extends InlineForm
{
    static function create(CMFResourceField $resourceField)
    {
        /** @var InlineResource $inlineForm */
        $inlineForm = parent::create($resourceField);
        /** @var AbstractObjectField $resourceField */
        $inlineForm->setFields($resourceField->getObjectFields());
        return $inlineForm;
    }
}