<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Resources\Contracts\CMFResource;
use LaravelCMF\Base\Resources\Fields\Field as ResourceField;
use LaravelCMF\Base\Resources\Fields\Relation\AbstractFieldRelation;

class Form
{
    protected $formId;
    protected $config;

    /**
     * @var Form[]
     */
    protected $tabs = [];

    /**
     * @var Group[]
     */
    protected $groups = [];

    /**
     * @var FieldInterface[]
     */
    protected $fields = [];

    /**
     * @param CMFResource $adminResource
     * @return Form
     */
    public static function create($formId, array $config)
    {

        //Merge in two
        $form = new self();
        $form->setFormId($formId)->setConfig($config);
        return $form;
    }

    public function getFormId()
    {
        return $this->formId;
    }

    public function getMethod()
    {
        //return POST
        return 'POST';
    }

    public function getAction()
    {
        //return the action this form will post to
        return '';
    }

    public function getEnctype()
    {
        //check if we have any 'upload' fields...
        return 'multipart/form-data';
    }

    /**
     * @return FieldInterface[]
     */
    public function getFields()
    {
        if (!$this->fields) {
            $resourceFields = $this->getConfig('fields', []);
            /** @var ResourceField $resourceField */
            foreach ($resourceFields as $resourceField) {
                //Skip if there is no form template since it is not meant for form displays.
                if(!$resourceField->getFormTemplate()) continue;

                //Our field is either a scalar, an object or another Resource.
                if($resourceField->getType() === ResourceField::TYPE_SCALAR) {
                    $field = InlineField::create($resourceField);
                    $this->fields[] = $field;
                } else if ($resourceField->getType() === ResourceField::TYPE_RELATION){
                    /** @var AbstractFieldRelation $resourceField */
                    $this->fields[] = InlineResource::create($resourceField);
                } else if ($resourceField->getType() === ResourceField::TYPE_OBJECT){
                    /** @var AbstractFieldRelation $resourceField */
                    $this->fields[] = InlineObject::create($resourceField);
                } else {
                    throw new \Exception('Unrecognised Field Type');
                }
            }
        }

        return $this->fields;
    }

    public function getTabs()
    {
        if(!$this->tabs) {
            $tabs = $this->getConfig('tabs', []);
            foreach($tabs as $tabKey => $tabSettings) {
                if(is_numeric($tabKey)) {
                    $tabKey = $tabSettings;
                }
                if(is_string($tabSettings)) {
                    $tabSettings = ['title' => $tabSettings];
                }
                $this->tabs[$tabKey] = new Tab($tabKey, $tabSettings);
            }
            $groups = $this->getGroups();
            foreach($groups as $group) {
                if(!isset($this->tabs[$group->tab])) {
                    $this->tabs[$group->tab] = new Tab($group->tab);
                }
                $this->tabs[$group->tab]->addGroup($group);
            }
        }
        return $this->tabs;
    }

    /**
     * @return Group[]
     */
    public function getGroups()
    {
        if (!$this->groups) {

            $groups = $this->getConfig('groups', []);
            foreach($groups as $groupKey => $groupSettings) {
                $this->groups[$groupKey] = new Group($groupKey, $groupSettings);
            }

            $fields = $this->getFields();
            foreach ($fields as $field) {
                $groupKey = $field->getGroup();

                /** @var $group = Group */
                $group = array_filter($this->groups, function (Group $fieldGroup) use ($groupKey) {
                    return $fieldGroup->getKey() === $groupKey;
                });
                if (!empty($group)) {
                    $group = current($group);
                } else {
                    $group                                   = new Group($groupKey);
                    $this->groups[$groupKey] = $group;
                }

                $group->addField($field);

            }
        }
        return $this->groups;
    }

    /**
     * @return mixed
     */
    public function getConfig($key = null, $default = null)
    {
        if($key) {
            return !empty($this->config[$key]) ? $this->config[$key] : $default;
        }
        return $this->config;
    }

    /**
     * @param mixed $config
     * @return Form
     */
    public function setConfig($config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @param mixed $formId
     * @return Form
     */
    public function setFormId($formId)
    {
        $this->formId = $formId;
        return $this;
    }



}