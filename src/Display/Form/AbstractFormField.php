<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Resources\Contracts\CMFResource;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;

abstract class AbstractFormField implements FieldInterface
{

    /**
     * @var CMFResourceField
     */
    protected $resourceField;
    protected $group = 'main';
    protected $fieldId;
    protected $fieldName;
    protected $fieldTitle;

    public static function create(CMFResourceField $resourceField)
    {
        $calledClass = get_called_class();
        /** @var AbstractFormField $formField */
        $formField = new $calledClass($resourceField);
        $title     = $resourceField->getSetting('title', str_replace("_", " ", $resourceField->getFieldKey()));
        $formField->setFieldTitle($title)->setFieldName($resourceField->getFieldName())->setFieldId($resourceField->getFieldId())
            ->setGroup($resourceField->getSetting('group', 'main'));

        return $formField;
    }

    /**
     * Field constructor.
     * @param CMFResourceField $resourceField
     */
    public function __construct(CMFResourceField $resourceField)
    {

        $this->resourceField = $resourceField;
    }

    public function render()
    {
        return CMFView($this->getResourceField()->getFormTemplate(),
            $this->getTemplateParameters())
            ->render();
    }

    public function getTemplateParameters()
    {
        return [
            'field' => $this,
            'resourceField' => $this->getResourceField()
        ];
    }

    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param string $group
     * @return $this
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return CMFResourceField
     */
    public function getResourceField()
    {
        return $this->resourceField;
    }

    /**
     * @return mixed
     */
    public function getFieldId()
    {
        return $this->fieldId;
    }

    /**
     * @param mixed $fieldId
     * @return $this
     */
    public function setFieldId($fieldId)
    {
        $this->fieldId = $fieldId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param mixed $fieldName
     * @return $this
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFieldTitle()
    {
        return $this->fieldTitle;
    }

    /**
     * @param mixed $fieldTitle
     * @return $this
     */
    public function setFieldTitle($fieldTitle)
    {
        $this->fieldTitle = $fieldTitle;
        return $this;
    }
}