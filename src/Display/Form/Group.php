<?php

namespace LaravelCMF\Base\Display\Form;

class Group
{
    /** @var  string */
    protected $key;

    /** @var array  */
    protected $settings = [];

    public $tab = 'main';

    /** @var FieldInterface */
    protected $fields = [];

    /**
     * Group constructor.
     * @param string $key
     * @param array $settings
     */
    public function __construct($key, $settings = [])
    {
        $this->key = $key;
        $this->settings = $settings;

        if(isset($settings['tab'])) {
            $this->tab = $settings['tab'];
        }
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    public function addField(FieldInterface $field)
    {
        $this->fields[] = $field;
    }

    /**
     * @return FieldInterface
     */
    public function getFields()
    {
        return $this->fields;
    }

    public function getTitle()
    {
        return ucfirst($this->key);
    }

}