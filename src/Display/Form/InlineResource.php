<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Display\Form\InlineForm;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;
use LaravelCMF\Base\Resources\Fields\FieldResource;
use LaravelCMF\Base\Resources\Fields\Relation\AbstractFieldRelation;

class InlineResource extends InlineForm
{
    static function create(CMFResourceField $resourceField)
    {
        /** @var InlineResource $inlineForm */
        $inlineForm = parent::create($resourceField);
        /** @var AbstractFieldRelation $resourceField */
        $inlineForm->setFields($resourceField->getFields());
        return $inlineForm;
    }
}