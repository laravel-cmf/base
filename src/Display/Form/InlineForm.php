<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;
use LaravelCMF\Base\Resources\Fields\FieldResource;

abstract class InlineForm extends AbstractFormField
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var ResourceModel
     */
    protected $value;

    /**
     * @var FieldInterface[]
     */
    protected $fields;

    /**
     * @var FieldResource $resourceField
     */
    protected $resourceField;

    /**
     * @var string
     */
    protected $group;

    public function getTemplateParameters()
    {
        return [
            'field' => $this,
            'fields' => $this->getFields(),
            'resourceField' => $this->getResourceField()
        ];
    }

    public function getField($fieldKey)
    {
        $matchingFields = array_filter($this->getFields(), function(FieldInterface $formField) use($fieldKey) {
            return $formField->getResourceField()->getFieldKey() === $fieldKey;
        });
        return count($matchingFields) === 1 ? current($matchingFields) : null;
    }

    /**
     * @return FieldInterface[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param FieldInterface[] $fields
     * @return InlineForm
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @return FieldResource
     */
    public function getResourceField()
    {
        return $this->resourceField;
    }

    /**
     * @param FieldResource $resourceField
     * @return InlineForm
     */
    public function setResourceField($resourceField)
    {
        $this->resourceField = $resourceField;
        return $this;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     * @return InlineForm
     */
    public function setGroup($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     * @return InlineForm
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

}