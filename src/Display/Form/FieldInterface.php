<?php

namespace LaravelCMF\Base\Display\Form;

use LaravelCMF\Base\Resources\Contracts\CMFResourceField;

interface FieldInterface
{
    public function render();
    public function getGroup();
    public function setGroup($group);
    public function getFieldTitle();
    public function setFieldTitle($fieldTitle);
    public function getFieldId();
    public function setFieldId($fieldId);
    public function getFieldName();
    public function setFieldName($fieldName);

    /**
     * @return CMFResourceField
     */
    public function getResourceField();
}