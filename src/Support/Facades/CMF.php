<?php

namespace LaravelCMF\Base\Support\Facades;

use Illuminate\Support\Facades\Facade;

class CMF extends Facade {

    protected static function getFacadeAccessor() { return 'CMF'; }

}