<?php

namespace LaravelCMF\Base\Support;

use Illuminate\Database\Schema\Blueprint;

class BaseDatabaseMigrations
{

    public static function nodeMigrations(Blueprint $table)
    {
        $table->increments('id');
        $table->string('title')->max(255);
        $table->unsignedInteger('node_id');
        $table->string('node_type')->max(255)->nullable();

        $table->unsignedInteger('_lft');
        $table->unsignedInteger('_rgt');
        $table->unsignedInteger('parent_id');

        $table->timestamps();
    }
}