<?php

namespace LaravelCMF\Base\Models\Eloquent;

abstract class BaseNode extends BaseModel
{
    /**
     * An instance of our superclass base node.
     */
    protected $baseNode;

    protected $listenersRegistered;
    public static $_icon = 'fa-object-group';

    /**
     * The class that is our base node.
     * @var
     */
    static $_baseClass;

    /**
     * Fields that belong in the base class.
     * @var array
     */
    public static $_baseFields = [
        'title', 'parent'
    ];

    public static $_displayField = 'title';

       public static $_fields = [
        'title' => [
            'field' => 'text'
        ],
        'parent' => [
            'field' => 'ParentNode',
            //'relation' =>  // <- not set assume self.
        ]
    ];

    public static $_listFields = ['title', 'parent'];



    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    final public function node()
    {
        return $this->morphTo();
    }

    public function base()
    {
        return $this->morphOne($this->getBaseClass(), 'node')->getResults();
    }

    final public function getBaseClass()
    {
        if(empty(static::$_baseClass) || !class_exists(static::$_baseClass))
            throw new \Exception('Base class was not defined or does not exist.');

        return static::$_baseClass;
    }

    public function setAttribute($key, $value)
    {
        //Set base attributes specifically on our base node.
        if(in_array($key, static::$_baseFields)) {
            return $this->setBaseAttribute($key, $value);
        }
        return parent::setAttribute($key, $value);
    }

    public function getAttribute($key)
    {
        //Set base attributes specifically on our base node.
        if(in_array($key, static::$_baseFields)) {
            return $this->getBaseAttribute($key);
        }
        return parent::getAttribute($key);
    }

    public function delete()
    {
        $baseNode = $this->base();
        if($baseNode && $this !== $baseNode)
            $baseNode->delete();

        return parent::delete(); // TODO: Change the autogenerated stub
    }


    /**
     * Convenience method to retrieve a value on the base node.
     * @param $key
     * @return mixed
     */
    final public function getBaseAttribute($key)
    {
        $baseNode = $this->getBaseNode();
        if($baseNode === $this) {
            return parent::getAttribute($key);
        } else {
            return $baseNode->getBaseAttribute($key);
        }
    }

    /**
     * Convenience method to set a value on the base node.
     * @param $key
     * @param $value
     * @return mixed
     */
    final public function setBaseAttribute($key, $value)
    {
        $baseNode = $this->getBaseNode();
        if($baseNode === $this) {
            return parent::setAttribute($key, $value);
        } else {
            $baseNode->setBaseAttribute($key, $value);
        }
    }

    /**
     * @return static::$_baseClass
     */
    final public function getBaseNode()
    {
        if (is_null($this->baseNode)) {
            $calledClass = get_called_class();
            $baseClass   = $this->getBaseClass();
            if ($calledClass !== $baseClass) {
                $baseNode = $this->exists ? $this->base() : new $baseClass;
                $this->baseNode = $baseNode;
            } else {
                $this->baseNode = $this;
            }
        }
        return $this->baseNode;
    }

    public function attachToParent(BaseNode $parent)
    {
        if(!$this->exists) {
            //add listener
            $this->getBaseNode()->created(function($baseNode) use($parent){
                $parent->appendNode($baseNode);
            });
        } else {
            return $parent->appendNode($this->getBaseNode());
        }
    }

}