<?php

namespace LaravelCMF\Base\Models\Eloquent;


class Setting extends BaseModel
{
    protected $fillable = ['title', 'key', 'value'];
    public $table = 'cmf_settings';

    public static $_listFields = [
        'title', 'key'
    ];
    public static $_icon = 'fa-cogs';

    public static $_fields = [
        //key is the attribute/property name unless overridden.
        'title' => [
        ],
        'key' => [
            'validation' => 'required'
        ],
        'value' => [
            'validation' => 'required',
            'field' => 'textarea'
        ],
    ];

}
