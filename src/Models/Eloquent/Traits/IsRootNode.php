<?php

namespace LaravelCMF\Base\Models\Eloquent\Traits;

use Kalnoy\Nestedset\NodeTrait;

trait IsRootNode
{
    use NodeTrait;
}