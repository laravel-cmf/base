<?php

namespace LaravelCMF\Base\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use LaravelCMF\Base\Models\Contracts\ResourceModel;

abstract class BaseModel extends Model implements ResourceModel
{
    //todo casts?
    public function getCasts()
    {
        return parent::getCasts();
    }

    public function getResourceIdentifier()
    {
        return $this->getKeyName();
    }

    public function getProperty($key)
    {
        return $this->getAttribute($key);
    }

    //todo a unified set/get method
    public function setProperty($key, $value)
    {
        $setMethod = 'set'.Str::studly($key);
        if(method_exists($this, $setMethod)) {
            $this->{$setMethod}();
        }
        return $this->setAttribute($key, $value);
    }
}
