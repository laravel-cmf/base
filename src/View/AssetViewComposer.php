<?php

namespace LaravelCMF\Base\View;

use Illuminate\View\View;
use LaravelCMF\Base\Modules\Manager;

class AssetViewComposer
{
    /**
     * @var Manager
     */
    private $moduleManager;

    /**
     * Create a new profile composer.
     *
     * @param Manager $moduleManager
     */
    public function __construct(Manager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $assets = $this->moduleManager->getModuleAssets();
        $scripts = isset($assets['scripts']) ? $assets['scripts'] : [];
        $view->with('cmf_script_assets', $scripts);
        $styles = isset($assets['styles']) ? $assets['styles'] : [];
        $view->with('cmf_style_assets', $styles);
    }
}