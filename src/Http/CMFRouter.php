<?php

namespace LaravelCMF\Base\Http;

use Illuminate\Routing\Router;
use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Http\Middleware\CMFAuthenticate;
use LaravelCMF\Base\Http\Middleware\CMFSettings;
use LaravelCMF\Base\Modules\Manager;
use LaravelCMF\Base\Resources\Registry;

class CMFRouter
{
    protected $prefix;

    protected $namespace = 'LaravelCMF\Base\Http\Controllers';

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var Registry
     */
    protected $registry;

    protected $resourceRoutes = [
        [
            'url' => '{resourceModel}',
            'action' => 'index',
            'methods' => ['get']
        ],
        [
            'url' => '{resourceModel}/create',
            'action' => 'create',
            'methods' => ['get']
        ],
        [
            'url' => '{resourceModel}/create',
            'action' => 'store',
            'methods' => ['post']
        ],
        [
            'url' => '{resourceModel}/{resourceId}',
            'action' => 'show',
            'methods' => ['get']
        ],
        [
            'url' => '{resourceModel}/{resourceId}/action',
            'action' => 'action',
            'methods' => ['get']
        ],
        [
            'url' => '{resourceModel}/{resourceId}/edit',
            'action' => 'edit',
            'methods' => ['get']
        ],
        [
            'url' => '{resourceModel}/{resourceId}/edit',
            'action' => 'update',
            'methods' => ['post', 'put', 'patch']
        ],
        [
            'url' => '{resourceModel}/{resourceId}/delete',
            'action' => 'delete',
            'methods' => ['get']
        ],
    ];

    protected $authRoutes = [
        [
            'url' => 'login',
            'action' => 'showLoginForm',
            'methods' => ['get']
        ],
        [
            'url' => 'logout',
            'action' => 'logout',
            'methods' => ['get']
        ],
        [
            'url' => 'login',
            'action' => 'login',
            'methods' => ['post']
        ],
        [
            'url' => 'password/reset/{token?}',
            'action' => 'showResetForm',
            'methods' => ['get'],
            'controller' => 'Auth\PasswordController'
        ],
        [
            'url' => 'password/email',
            'action' => 'sendResetLinkEmail',
            'methods' => ['post'],
            'controller' => 'Auth\PasswordController'
        ],
        [
            'url' => 'password/reset',
            'action' => 'reset',
            'methods' => ['post'],
            'controller' => 'Auth\PasswordController'
        ],
    ];

    protected $middlewareGroups = [
        'cmf_settings' => [
            CMFSettings::class,
        ],
        'cmf_admin' => [
            CMFAuthenticate::class
        ]
    ];

    /**
     * CMFRouter constructor.
     * @param Router $router
     * @param Manager $manager
     * @param Registry $registry
     */
    public function __construct(Router $router, Manager $manager, Registry $registry)
    {
        $this->prefix   = CMF::configGet('cmf.prefix');
        $this->router   = $router;
        $this->manager  = $manager;
        $this->registry = $registry;
        foreach ($this->middlewareGroups as $name => $group) {
            $router->middlewareGroup($name, $group);
        }
    }

    public function mapRoutes()
    {
        $this->registerModuleRoutes();
        $this->registerAuthRoutes();
        $this->registerResourceRoutes();
        $this->registerBaseRoutes();
        $this->registerUploadRoutes();
    }

    public function registerModuleRoutes()
    {
        //Use all registered modules
        $modules = $this->manager->getModules();
        foreach ($modules as $module) {
            $module->mapRoutes($this->router);
        }
    }

    public function registerResourceRoutes()
    {
        $this->router->group([
            'prefix' => $this->prefix,
            'middleware' => ['web', 'cmf_settings', 'cmf_admin'],
            'namespace' => $this->namespace
        ], function ($router) {
            foreach ($this->resourceRoutes as $resourceRoute) {
                $router->match(['GET'],
                    '',
                    ['uses' => 'DashboardController@index']
                );

                $router->match($resourceRoute['methods'],
                    $resourceRoute['url'],
                    ['uses' => 'ResourceController@' . $resourceRoute['action']]
                );
            }
        });
    }

    public function registerAuthRoutes()
    {
        $this->router->group([
            'prefix' => $this->prefix,
            'middleware' => ['web', 'cmf_settings'],
            'namespace' => $this->namespace
        ], function ($router) {
            foreach ($this->authRoutes as $authRoute) {
                $router->match($authRoute['methods'],
                    $authRoute['url'],
                    ['uses' => 'Auth\AuthController@' . $authRoute['action']]
                );
            }
        });
    }

    /**
     * Method to register the admin resource and asset routes.
     * @param Router $router
     */
    public function registerUploadRoutes()
    {
        $this->router->group([
            'prefix' => 'uploads',
            'namespace' => $this->namespace,
            'middleware' => ['web'],
        ], function ($router) {
            $router->match(['GET'],
                '{filePath}',
                ['uses' => 'AssetController@getUpload']
            )->where('filePath', '(.*)');
        });
    }

    public function registerBaseRoutes()
    {
        $this->router->group([
            'prefix' => $this->prefix,
            'middleware' => ['web'],
            'namespace' => $this->namespace
        ], function ($router) {
            //register INSTALL
            $router->match(['GET'],
                'install',
                ['uses' => 'Auth\AuthController@getRegister']
            );
            $router->match(['POST'],
                'install',
                ['uses' => 'Auth\AuthController@postRegister']
            );
            //register ASSETS
            $router->match(['GET'],
                'assets/{filePath}',
                ['uses' => 'AssetController@getAsset']
            )->where('filePath', '(.*)');

            //Authentication required for these routes.
//            $this->router->group([
//                'middleware' => ['cmf_settings','cmf_admin'],
//                'namespace' => $this->namespace
//            ], function ($router) {
//                //register DASHBOARD
//                $router->match(['GET'],
//                    '/',
//                    ['uses' => 'DashboardController@index']
//                );
//                //register 404?
//            });


        });
    }

}