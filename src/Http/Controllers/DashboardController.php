<?php

namespace LaravelCMF\Base\Http\Controllers;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\Fields\Relation\ManyToMany;
use LaravelCMF\Base\Resources\FormBuilder;
use LaravelCMF\Base\Resources\Registry;
use LaravelCMF\Base\Resources\Repository;

class DashboardController extends BaseController
{

    /**
     * GET /adminPrefix
     * Display a landing page for the admin system.
     */
    public function index()
    {
        return CMFView('admin.dashboard');
    }
}