<?php namespace LaravelCMF\Base\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use LaravelCMF\Base\Auth\CMFAuth;
use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Http\CMFRouter;
use LaravelCMF\Base\Modules\Manager;
use LaravelCMF\Base\Resources\Registry;
use LaravelCMF\Base\Services\AssetService;
use LaravelCMF\Base\View\AssetViewComposer;
use LaravelCMF\Base\View\AuthViewComposer;
use LaravelCMF\Base\View\NavigationViewComposer;
use LaravelCMF\Base\View\SettingsViewComposer;

class CMFProvider extends ServiceProvider
{

    /**
     * @var Manager
     */
    protected $moduleManager;
    protected $publishedConfigs = [
        'cmf', 'modules', 'models'
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Publish items first
        $publishedConfigs = [];
        foreach($this->publishedConfigs as $config) {
            $publishedConfigs[__DIR__ . "/../../config/{$config}.php"] = config_path(CMF::PACKAGE_NAME."/{$config}.php");
        }

        $this->publishes($publishedConfigs);

        $this->publishes([
            __DIR__.'/../../database/migrations/' => database_path('migrations')
        ], 'migrations');

        //Register base view composers before
        $this->registerViewComposer('*',  [
            AssetViewComposer::class,
            SettingsViewComposer::class,
            NavigationViewComposer::class,
        ]);

        //Boot modules, they may provide view locations etc.
        $this->getModuleManager()->bootModules();

        Registry::instance()->createListeners()->createStaticResources();

        //Load views from other modules first, incase they override the views in this module
        //Use the same namespace for all views
        $this->loadViewsFrom(
            __DIR__ . '/../../resources/views',
            CMF::PACKAGE_NAME);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/../Support/helpers.php';

        //Base config contains user defined values
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/cmf.php', CMF::configKey('cmf')
        );

        $this->mergeConfigFrom(
            __DIR__ . '/../../config/models.php', CMF::configKey('models')
        );


        //Setup all CMF Modules that have been declared in the base application
        $modules = $this->getRegisteredModules();
        $moduleManager = $this->getModuleManager();
        $this->app->instance(Manager::class, $moduleManager);

        //Register the modules
        foreach($modules as $module) {
            $moduleManager->registerModule(app($module));
        }

        $this->mergeModuleConfigs();

        //Setup Resource Registry which contains all Resources registered for the CMF.
        $this->app->instance(Registry::class, app(Registry::class));

        //Register Routes. Need to register for all Resource Models and for any Modules that offer custom routes
        $cmfRouter = $this->app->make(CMFRouter::class);
        $cmfRouter->mapRoutes();

        //Register AUTH settings - todo this goes in the admin module
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/auth.php', 'auth', true
        );

        //Register CMF instance.
        $this->app->instance(CMF::class, CMF::instance());
        $this->app->bind('CMF', CMF::class);
        //CMF Auth
//        $this->app->instance(CMFAuth::class, CMFAuth::instance());
//        $this->app->instance('CMFAuth', CMFAuth::instance());
//        dd(app('CMFAuth'));
        //boot the model registry

//        $this->app->bind('CMFAuth', function ($app) {
//            return $app['auth']::guard(CMF::GUARD);
//        });
    }

    public function registerViewComposer($templateKey, $callbacks)
    {
        //Package name is our view namespace
        $templateKey = CMF::PACKAGE_NAME . "::" . $templateKey;
        if(!is_array($callbacks)) {
            $callbacks = [$callbacks];
        }

        foreach($callbacks as $callback) {
            view()->composer($templateKey, $callback);
        }
    }

    protected function mergeModuleConfigs()
    {
        $mergedConfig = [];

        foreach($this->getModuleManager()->getModules() as $module) {
            $config = $module->getConfig();
            if(!empty($config)) {
                $mergedConfig = array_merge_recursive($mergedConfig, $config);
            }
        }

        foreach($mergedConfig as $key => $config) {
            $configKey = CMF::configKey($key);
            $currentConfig = $this->app['config']->get($configKey, []);
            $this->app['config']->set($configKey, array_unique(array_merge($config, $currentConfig)));
        }

    }
    protected function getRegisteredModules()
    {
        return $this->app['config']->get(CMF::configKey('cmf.modules'), []);
    }

    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param  string $path
     * @param  string $key
     * @param bool $replace
     */
    public function mergeConfigFrom($path, $key, $replace = false)
    {
        /**
         * We have default config settings such as our MODELS
         *
         * In our modules we have some new settings that may need to be merged/updated on the base application
         * -- HOW DO WE WRITE NEW CONFIG SETTINGS TO PREVIOUSLY CREATED CONFIG FILES?
         * ---- manually?
         * --
         *
         * perhaps modules have there own models config that declare certain settings?
         *
         */
        $config = $this->app['config']->get($key, []);

        $defaultConfig = require $path;

        if($replace) {
            $mergedConfig = array_replace_recursive($defaultConfig, $config);
        } else {
            $mergedConfig = array_merge($defaultConfig, $config);
        }

        $this->app['config']->set($key, $mergedConfig);
    }

    /**
     * @return Manager
     */
    public function getModuleManager()
    {
        if(!$this->moduleManager){
            $this->moduleManager = new Manager($this);
        }
        return $this->moduleManager;
    }

    public function publishes(array $paths, $group = null)
    {
        parent::publishes($paths, $group);
    }

    /**
     * Register a view file namespace.
     *
     * @param  string  $path
     * @param  string  $namespace
     * @return void
     */
    public function loadViewsFrom($path, $namespace)
    {
        parent::loadViewsFrom($path, $namespace);
    }

}
