<?php

namespace LaravelCMF\Base\Modules;

use LaravelCMF\Base\Providers\CMFProvider;

class Manager
{

    /**
     * @var ModuleInterface[]
     */
    protected $modules = [

    ];

    /** @var  CMFProvider */
    protected $cmfServiceProvider;

    /**
     * Manager constructor.
     * @param CMFProvider $cmfServiceProvider
     */
    public function __construct(CMFProvider $cmfServiceProvider)
    {
        $this->cmfServiceProvider = $cmfServiceProvider;
        //Add the base module
        $this->addModule(new BaseModule());
    }


    public function addModule(ModuleInterface $module)
    {
        if(!$this->getModule($module->getModuleName()))
            $this->modules[] = $module;

        return $module;
    }

    public function getModule($moduleName)
    {
        return isset($this->modules[$moduleName]) ? $this->modules[$moduleName] : null;
    }

    /**
     * @return ModuleInterface[]
     */
    public function getModules()
    {
        return $this->modules;
    }

    public function registerModule(ModuleInterface $module)
    {
        $this->addModule($module)->register($this->cmfServiceProvider);
    }

    public function getModuleAssets()
    {
        $assets = [];

        foreach($this->modules as $module) {
            if(empty($module->getAssets())) continue;
            $assets = array_merge_recursive($assets, $module->getAssets());
        }

        return $assets;
    }

    public function bootModules()
    {
        foreach($this->getModules() as $module) {
            $module->boot($this->cmfServiceProvider);
        }
    }
}