<?php

namespace LaravelCMF\Base\Modules;

use Illuminate\Routing\Router;
use LaravelCMF\Base\Providers\CMFProvider;

interface ModuleInterface
{
    public function getModuleName();
    public function boot(CMFProvider $serviceProvider);
    public function register(CMFProvider $serviceProvider);
    public function mapRoutes(Router $router);
    public function getAssets();
    public function getConfig();

}