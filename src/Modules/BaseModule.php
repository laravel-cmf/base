<?php

namespace LaravelCMF\Base\Modules;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use LaravelCMF\Base\Providers\CMFProvider;

class BaseModule implements ModuleInterface
{
    protected $assets = [
        'scripts' => [
        ],
        'styles' => [
            "https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"
        ]
    ];
    protected $styles = [

    ];

    public function getModuleName()
    {
        return 'cmf-base';
    }

    public function boot(CMFProvider $serviceProvider)
    {

    }

    public function register(CMFProvider $serviceProvider)
    {

    }

    public function mapRoutes(Router $router)
    {

    }

    public function getAssets()
    {
        $assets = array_merge_recursive(
            $this->assets, [
            'scripts' => [
                cmf_asset("js/cmf.app.js"),
            ],
            'styles' => [
                cmf_asset("css/vendor.css")
            ]
        ]);
        return $assets;
    }

    public function getConfig()
    {
        return require __DIR__ . '/../../config/module.php';
    }
}