<?php

namespace LaravelCMF\Base\Resources\Contracts;

use LaravelCMF\Base\Models\Contracts\ResourceModel;

interface CMFResource
{
    public function getResourceKey();
    public function getResourceModel();
    public function setResourceModel(ResourceModel $resourceModel);
    public function getScopedResourceKey($childScopedResourceKey = null);
}