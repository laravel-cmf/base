<?php

namespace LaravelCMF\Base\Resources\Contracts;


use Illuminate\Http\Request;
use LaravelCMF\Base\Models\Contracts\ResourceModel;

interface CMFResourceField
{
    public function getType();

    public function getFieldKey();

    public function setFieldKey($fieldKey);

    /**
     * @param string $resourceKey
     * @return CMFResourceField
     */
    public function setResourceKey($resourceKey);

    /**
     * @return mixed
     */
    public function getResourceKey();

    public function getFieldPosition();
    public function setFieldPosition($fieldPosition);

    /**
     * Pre-process any settings.
     * @param $settings
     * @return $settings
     */
    public function processSettings($settings);

    /**
     * Pre-process the field value.
     * @param $fieldValue
     * @return $fieldValue
     */
    public function processFieldValue($fieldValue);

    /**
     * Return a scalar value that displays on the frontend.
     * @return mixed
     */
    public function display();

    /**
     * Return a processed value from the persisted data.
     * @return mixed
     */
    public function value();

    /**
     * @return bool
     */
    public function valid();

    /**
     * @return bool
     */
    public function dirty();

    /**
     * Parse and validate Request data and set processed value.
     * @param Request $request
     */
    public function processRequest(Request $request);

    public function updateField();

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @param array $errors
     * @return CMFResourceField
     */
    public function setErrors($errors);

    /**
     * Return value as exists in persistence.
     * @return mixed
     */
    public function getFieldValue();

    /**
     * @param mixed $fieldValue
     * @return CMFResourceField
     */
    public function setFieldValue($fieldValue);

    public function getSetting($key, $default = null);

    /**
     * @return string
     */
    public function getFormTemplate();

    public function getRequestKey();

    /**
     * @return CMFResource
     */
    public function getResource();

    /**
     * @param CMFResource $resource
     * @return self
     */
    public function setResource($resource);

    public function getFieldName();

    public function getFieldId();

    /**
     * Return the transformed version of this field, included in array outputs etc.
     * @return mixed
     */
    public function transform();
}