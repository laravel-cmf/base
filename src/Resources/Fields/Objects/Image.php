<?php

namespace LaravelCMF\Base\Resources\Fields\Objects;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Resources\Fields\Values\ImageObject;

class Image extends AbstractObjectField
{
    /**
     * @var Filesystem
     */
    protected $disk;

    /** @var  UploadedFile */
    protected $fileUpload;

    protected $imageObject;

    protected $form_template = 'admin.fields.form.image';

    /**
     * Return the rendered value of this resource field to display in list views.
     * @return mixed
     */
    public function display()
    {
        $fileData = $this->value();
        if (!$fileData) {
            return '';
        }

        return '<img src="' . $fileData->src_url . '" alt="" width="100" />
        <p>'.$fileData->src_url.'</p>';
    }

    public function updateField()
    {
        //move files
        if ($this->dirty() && $this->fileUpload && $this->getFieldValue()) {
            $fileData = json_decode($this->getFieldValue(), true);

            $disk = $this->getDisk();
            $success = $disk->put($fileData['path'] . DIRECTORY_SEPARATOR . $fileData['file_name'],
                file_get_contents($this->fileUpload));
            if(!$success) {
                throw new \Exception('Unable to move image file.');
            }
            //delete old one
            $this->deleteOriginal();
        }
        parent::updateField();
    }

    public function processRequest(Request $request)
    {
        //get the file
        //move the file someplace
        //boom
        $fileData = null;
        $fileKey  = $this->getRequestKey() . '.file';
        $keepKey  = $this->getRequestKey() . '.keep';
        $file     = $request->file($fileKey, null);
        if ($file) {
            //possibly validate image sizes etc.?
            //check stuff on $data
            $fileData = [
                'original_name' => $file->getClientOriginalName(),
                'extension' => $file->getClientOriginalExtension(),
                'size' => $file->getClientSize(),
                'mime_type' => $file->getClientMimeType(),
            ];

            $filePath       = '/uploads/images/';
            $fileName       = $file->getClientOriginalName();
            $uniqueFileName = $this->getUniqueFilePath($filePath, $fileName);

            $fileData['path']      = $filePath;
            $fileData['file_name'] = $uniqueFileName;
            $fileData['src']       = $filePath . $uniqueFileName;
            $this->fileUpload      = $file;
            $this->imageObject = new ImageObject($fileData);
            $this->setFieldValue(json_encode($fileData));
        } else {
            if (!$request->input($keepKey)) {
                $this->fileUpload = null;
                $this->setFieldValue(null);
            }
        }
    }

    private function getUniqueFilePath($filePath, $fileName, $version = null)
    {
        $uniqueFileName = (!empty($version) ? $version . '_' : '') . $fileName;
        $file           = $filePath . $uniqueFileName;
        $disk           = $this->getDisk();
        if ($disk->exists($file)) {
            $uniqueFileName = $this->getUniqueFilePath($filePath, $fileName, $version + 1);
        }

        return $uniqueFileName;
    }

    public function value()
    {
        return $this->image();
    }

    /**
     * @return ImageObject|null
     */
    public function image()
    {
        if(!$this->imageObject) {

            if(is_a($this->getFieldValue(), ImageObject::class)) {
                $this->imageObject = $this->getFieldValue();
            } else {

                $imageArray = $this->fieldValue ? json_decode($this->fieldValue, true) : null;
                if (!empty($imageArray) && is_array($imageArray)) {
                    $this->imageObject = new ImageObject($imageArray);
                }
            }
        }

        return $this->imageObject;
    }

    /**
     * @return Filesystem
     */
    public function getDisk()
    {
        if (!$this->disk) {
            $this->disk = Storage::disk(CMF::configGet('cmf.disk', 'public'));
        }
        return $this->disk;
    }

    public function deleteOriginal()
    {
        $disk = $this->getDisk();
        if ($this->originalValue && is_a($this->originalValue, ImageObject::class) && $disk->exists($this->originalValue->src)) {
            $disk->delete($this->originalValue->src);
        }
    }

}