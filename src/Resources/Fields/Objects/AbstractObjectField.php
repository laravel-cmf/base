<?php

namespace LaravelCMF\Base\Resources\Fields\Objects;

use LaravelCMF\Base\Resources\Fields\Field;

abstract class AbstractObjectField extends Field
{
    protected $type = self::TYPE_OBJECT;

    //Some objects have their own fields to display in a form.
    public function getObjectFields()
    {
        return [];
    }
}