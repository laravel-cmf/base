<?php

namespace LaravelCMF\Base\Resources\Fields\Objects;

use Illuminate\Http\Request;

class ObjectField extends AbstractObjectField
{
    protected $form_template = 'admin.fields.form.object';

    public function extractFieldData($fieldData, &$fieldValue)
    {
        foreach ($fieldData as $fieldKey => $objectValue) {
            if (isset($objectValue['settings'])) {
                $settings = $objectValue['settings'];
                foreach ($settings as $key => $val) {
                    $this->getResource()->setResourceFieldSetting($this->getFieldKey() . '.' . $fieldKey . '.' . $key,
                        $val);
                }
                $objectValue = $objectValue['value'];
            }
            $fieldValue[$fieldKey] = $objectValue;
        }
    }

    public function processRequest(Request $request)
    {
        if ($request->exists($this->getRequestKey())) {
            $fieldData  = $request->input($this->getRequestKey());
            $fieldValue = [];

            if($this->getSetting('multiple')) {
              $fieldValue = array_map(function($object) {
                  $data = [];
                  $this->extractFieldData($object, $data);
                  return $data;
              }, $fieldData);
            } else {
                $this->extractFieldData($fieldData, $fieldValue);
            }

            $this->setFieldValue(json_encode($fieldValue));
        }

    }

    public function value()
    {
        return $this->getFieldValue() ? json_decode($this->getFieldValue(), true) : [];
    }

    public function getFieldConfig()
    {
        $fieldSettings = $this->getSetting('fields', []);

        return $this->getObjectData($fieldSettings, []);
    }

    public function getObjectFields()
    {
        //todo make them more than just text or textarea fields and one level
        $object    = $this->value();
        $fields       = [];
        $fieldSettings = $this->getSetting('fields', []);
        if($this->getSetting('multiple')) {
            foreach ($object as $objectData) {
                $fields[] = $this->getObjectData($fieldSettings, $objectData);
            }
        } else {
            //only one of each.
            $fields = $this->getObjectData($fieldSettings, $object);
        }

        return $fields;
    }

    public function getObjectData($fieldSettings, $objectData)
    {
        $fields = [];
        foreach ($fieldSettings as $fieldKey => $config) {
            $fields[$fieldKey] = [
                'title' => isset($objectData['title']) ? $objectData['title'] : ucfirst(str_replace('_', ' ', $fieldKey)),
                'value' => isset($objectData[$fieldKey]) ? $objectData[$fieldKey] : '',
                'type' => !empty($config['field']) && in_array($config['field'], ['text', 'textarea', 'template']) ? $config['field'] : 'text',
                'template' => !empty($config['template']) ? $config['template'] : false,

            ];
        }
        return $fields;
    }

    public function transform()
    {
        if($this->getSetting('multiple')) {
            return array_map(function($objectItem){
                return $this->mapObjectValues($objectItem);
            }, $this->getObjectFields());
        } else {
            return $this->mapObjectValues($this->getObjectFields());
        }
    }

    public function mapObjectValues($object)
    {
        return array_map(function ($objectField) {
            return $objectField;
        }, $object);
    }
}