<?php

namespace LaravelCMF\Base\Resources\Fields\Computed;

use Illuminate\Http\Request;
use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Resources\Fields\Field;

class Template extends Field
{
    protected $form_template = 'admin.fields.form.template';

    public function processRequest(Request $request)
    {
        if ($request->exists($this->getRequestKey())) {
            $fieldData = $request->input($this->getRequestKey());
            $fieldValue = $fieldData['value'];
            $settings = isset($fieldData['settings']) ? $fieldData['settings'] : [];

            foreach($settings as $key => $val) {
                $this->getResource()->setResourceFieldSetting($this->getFieldKey().'.'.$key, $val);
            }
            $this->setFieldValue($fieldValue);
        }
    }

}