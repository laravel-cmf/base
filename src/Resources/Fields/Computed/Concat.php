<?php

namespace LaravelCMF\Base\Resources\Fields\Computed;

use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Resources\Fields\Field;

class Concat extends Field
{

    public function updateField()
    {
        $fields = $this->getSetting('fields', []);
        $separator = $this->getSetting('separator', "");
        $resourceFields = $this->getResource()->getResourceFields($fields);
        if($resourceFields) {
            $computedValue = implode($separator, array_map(function(Field $field) {
                return $field->getFieldValue();
            }, $resourceFields));
            $this->setFieldValue($computedValue);
            parent::updateField();
        }
    }


}