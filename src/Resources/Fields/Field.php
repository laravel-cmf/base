<?php

namespace LaravelCMF\Base\Resources\Fields;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\BaseResource;
use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Resources\Contracts\CMFResource;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;

class Field implements CMFResourceField
{
    const TYPE_SCALAR   = 'scalar';
    const TYPE_RELATION = 'relation';
    const TYPE_OBJECT   = 'object';

    protected $type = self::TYPE_SCALAR;

    /**
     * @param CMFResource $resource
     * @param $fieldKey
     * @param null $fieldValue
     * @param array $fieldSettings
     * @return self
     */
    public static function create(CMFResource $resource, $fieldKey, $fieldValue = null, $fieldSettings = [])
    {
        /** @var self $field */
        $class = get_called_class();
        $field = new $class();
        $field->setResource($resource)
            ->setResourceKey($resource->getResourceKey())
            ->setFieldKey($fieldKey)
            ->setOriginalValue($fieldValue)
            ->setFieldSettings($field->processSettings($fieldSettings))
            ->setFieldValue($field->processFieldValue($fieldValue));

        return $field;
    }

    /**
     * @var BaseResource
     */
    protected $resource;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * @var string
     */
    protected $fieldKey;

    /**
     * @var mixed
     */
    protected $fieldValue;

    /**
     * @var mixed
     */
    protected $originalValue;

    /**
     *
     * @var string
     */
    protected $resourceKey;

    /**
     * @var array
     */
    protected $fieldSettings;

    protected $form_template;

    /**
     * Used to define position within the other Resource fields.
     * @var int
     */
    protected $fieldPosition = 100;

    public function processSettings($settings)
    {
        return $settings;
    }

    public function processFieldValue($fieldValue)
    {
        if (is_null($fieldValue)) {
            return $this->getSetting('default', null);
        }
        return $fieldValue;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return empty($this->getErrors());
    }

    /**
     * @return bool
     */
    public function dirty()
    {
        return $this->originalValue !== $this->fieldValue;
    }

    /**
     * Requires update if the fieldValue has changed and it's not null, or we allow null values.
     * @return bool
     */
    public function requiresUpdate()
    {
        return ($this->dirty() &&
        (!is_null($this->fieldValue) || $this->getSetting('nullable')));
    }

    /**
     * Parse and validate Request data and set processed value.
     * @param Request $request
     */
    public function processRequest(Request $request)
    {
        if ($request->exists($this->getRequestKey())) {
            $processedValue = $request->input($this->getRequestKey(), null);
            $this->setFieldValue($processedValue);
        }
    }

    public function updateField()
    {
        if ($this->dirty() &&
            (!is_null($this->fieldValue) || $this->getSetting('nullable'))
        ) {
            $resourceModel = $this->getResource()->getResourceModel();
            $resourceModel->setProperty($this->getFieldKey(), $this->getFieldValue());
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $resourceKey
     * @return Field
     */
    public function setResourceKey($resourceKey)
    {
        $this->resourceKey = $resourceKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResourceKey()
    {
        return $this->resourceKey;
    }

    /**
     * @param string $fieldKey
     * @return Field
     */
    public function setFieldKey($fieldKey)
    {
        $this->fieldKey = $fieldKey;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFieldKey()
    {
        return $this->fieldKey;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     * @return Field
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
        return $this;
    }

    public function addError($error)
    {
        $this->setErrors($this->getErrors() + [$error]);
    }

    /**
     * Return value as exists in persistence.
     * @return mixed
     */
    public function getFieldValue()
    {
        return $this->fieldValue;
    }

    /**
     * @param mixed $fieldValue
     * @return Field
     */
    public function setFieldValue($fieldValue)
    {
        $this->fieldValue = $fieldValue;
        return $this;
    }

    /**
     * @param mixed $originalValue
     * @return Field
     */
    public function setOriginalValue($originalValue)
    {
        $this->originalValue = $originalValue;
        return $this;
    }

    /**
     * @param array $fieldSettings
     * @return Field
     */
    public function setFieldSettings($fieldSettings)
    {
        $this->fieldSettings = $fieldSettings;
        return $this;
    }

    /**
     * @return array
     */
    public function getFieldSettings()
    {
        return $this->fieldSettings;
    }

    public function getSetting($key, $default = null)
    {
        if (isset($this->getFieldSettings()[$key])) {
            return $this->getFieldSettings()[$key];
        }
        return $default;
    }

    /**
     * @return string
     */
    public function getFormTemplate()
    {
        return $this->form_template;
    }

    public function getRequestKey()
    {
        return $this->getFullResourceKey();
    }

    public function getFullResourceKey()
    {
        return $this->getResource()->getScopedResourceKey($this->getFieldKey());
    }

    /**
     * @return BaseResource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param CMFResource $resource
     * @return Field
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * Make a form field name from the dot notation resource field path.
     * @return string
     * @throws \Exception
     */
    public function getFieldName()
    {
        $keyParts = explode(".", $this->getFullResourceKey());
        if (empty($keyParts)) {
            throw new \Exception('There were no elements to generate a field name.');
        }
        $fieldName = array_shift($keyParts);
        if (!empty($keyParts)) {
            foreach ($keyParts as $part) {
                $fieldName .= "[" . $part . "]";
            }
        }

        return $fieldName;

    }

    public function getFieldId()
    {
        return str_replace(".", "-", $this->getFullResourceKey()) . '-resourceField';
    }

    /**
     * @return int
     */
    public function getFieldPosition()
    {
        return $this->fieldPosition;
    }

    /**
     * @param int $fieldPosition
     * @return Field
     */
    public function setFieldPosition($fieldPosition)
    {
        $this->fieldPosition = $fieldPosition;
        return $this;
    }

    public function display()
    {
        return $this->value();
    }

    /**
     * Return a processed value from the persisted data.
     * @return mixed
     */
    public function value()
    {
        return $this->getFieldValue();
    }

    public function transform()
    {
        return $this->value();
    }

}