<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

use LaravelCMF\Base\Resources\BaseResource;
use LaravelCMF\Base\Resources\Fields\Field;
use LaravelCMF\Base\Resources\Registry;

abstract class AbstractFieldRelation extends Field
{
    /**
     * This is a Resource type since it extends to a child BaseResource
     * @var string
     */
    protected $type = self::TYPE_RELATION;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var BaseResource
     */
    protected $relatedResource;

    public function getFields()
    {
        //return any fields that need to be handled on their own
        return [];
    }

    /**
     * @return \LaravelCMF\Base\Models\Contracts\ResourceModel
     */
    public function value()
    {
        return $this->getRelatedResource()->getResourceModel();
    }

    /**
     * @return string
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }

    /**
     * @param string $modelClass
     * @return FieldResource
     */
    public function setModelClass($modelClass)
    {
        $this->modelClass = $modelClass;
        return $this;
    }

    /**
     * @return BaseResource
     */
    public function getRelatedResource()
    {
        if(empty($this->relatedResource)) {
            $this->relatedResource = Registry::instance()->createResource($this->getModelClass(), $this->getResource());
        }
        return $this->relatedResource;
    }

    public function processSettings($settings)
    {
        $settings = parent::processSettings($settings);
        $relatedClass = isset($settings['relation']) ? $settings['relation'] : null;

        if(empty($relatedClass) || !class_exists($relatedClass)) {
            throw new \Exception('The related class does not exist for this field '.$this->getFieldKey());
        }
        $this->setModelClass($relatedClass);

        return $settings;
    }

    /**
     * Make sure we have a base resource using our related class.
     * @param $fieldValue
     * @return mixed|null
     * @throws \Exception
     */
    public function processFieldValue($fieldValue)
    {
        $baseResource = $this->getRelatedResource();
        $resourceModel = null;
        //todo we can leave it as null for ManyToOne or 'optional' relationships?
        if(is_null($fieldValue) && !$this->getSetting('nullable')) {
            //If fieldValue is null then make a new ResourceModel
            $value = $baseResource->instance();
        } else if (is_numeric($fieldValue)) {
            //If fieldValue is an ID then get the URL object from the repository
            $value = $baseResource->loadResourceModel($fieldValue);
        } else if (is_a($fieldValue, $this->getModelClass())) {
            //If the fieldValue is an $relatedClass..
            $baseResource->setResourceModel($fieldValue);
            $value = $baseResource->getResourceIdentifier();
        } else {
            throw new \Exception ('Could not find or create an instance of '.$this->getModelClass());
        }

        return parent::processFieldValue($fieldValue);
    }

    public function display()
    {
        //by default return the child resource display
        return $this->getRelatedResource()->display();
    }

    public function transform(){
        return $this->display();
    }
}