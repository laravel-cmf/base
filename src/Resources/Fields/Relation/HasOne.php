<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

class HasOne extends AbstractFieldRelation
{
    protected $form_template = 'admin.fields.inline-form';

    public function processSettings($settings)
    {
        $settings = parent::processSettings($settings);

        $settings['nullable'] = false;

        return $settings;
    }

    public function updateField()
    {
        if ($this->requiresUpdate()) {
            $this->getRelatedResource()->saveResourceModel();
            $resourceModel = $this->getRelatedResource()->getResourceModel();
            $resourceModel->setProperty($this->getFieldKey(), $this->value());
        }
    }

    public function getFields()
    {
        return $this->getRelatedResource()->getForm()->getFields();
    }

    public function transform(){
        return $this->getRelatedResource()->transform();
    }
}