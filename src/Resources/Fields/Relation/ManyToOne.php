<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

use LaravelCMF\Base\Resources\BaseResource;
use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Resources\Fields\Select;
use LaravelCMF\Base\Resources\Registry;
use LaravelCMF\Base\Resources\Repository;

class ManyToOne extends AbstractFieldRelation
{
    protected $form_template = 'admin.fields.form.select';

    public function getOptions()
    {
        //add in the related data as options
        if($relation = $this->getSetting('relation')) {
            //we have a related model so let's get the options set up
            $repository = app(Repository::class);

            $models = $repository->findAll($relation);

            //Check it's not related to itself, we want to remove this entry from the options
            if($relation === $this->getResource()->getClassName() && $this->getResource()->getResourceIdentifier()) {
                $models = array_filter($models, function(BaseResource $model) {
                    return $model->getResourceIdentifier() !== $this->getResource()->getResourceIdentifier();
                });
            }

            //Add nullable option
            $options = [
                [
                    'label' => 'Select '.$this->getRelatedResource()->singular(),
                    'value' => '',
                    'selected' => false
                ]
            ];
            /** @var BaseResource $model */
            foreach($models as $model) {
                $options[] = ['label' => $model->display(), 'value' => $model->getResourceIdentifier(),
                              'selected' => $model->getResourceIdentifier() == $this->getFieldValue()];
            }
            return $options;
        }
        return null;
    }

    public function processFieldValue($fieldValue)
    {
        if($fieldValue) {
            $baseResource = $this->getRelatedResource();
            if (is_numeric($fieldValue)) {
                //If fieldValue is an ID then get the URL object from the repository
                $fieldValue = $baseResource->loadResourceModel($fieldValue);
            } else if (is_a($fieldValue, $this->getModelClass())) {
                //If the fieldValue is an $relatedClass..
                $baseResource->setResourceModel($fieldValue);
                $fieldValue = $baseResource->getResourceIdentifier();
            } else {
                throw new \Exception ('Could not find or create an instance of '.$this->getModelClass());
            }
        }
        return parent::processFieldValue($fieldValue);
    }

    public function processSettings($settings)
    {
        $settings = parent::processSettings($settings);

        $settings['nullable'] = true;

        return $settings;
    }

    public static function transformFieldValue($fieldValue = null)
    {
        //transform field value into what's required
        if($fieldValue) {
            if(is_object($fieldValue) && in_array(ResourceModel::class, class_implements($fieldValue))) {
                //this is an object
                $fieldValue = new BaseResource($fieldValue);
            } else if(is_scalar($fieldValue)) {
                //could be a plain ID.
                //.....
            }
        }

        return $fieldValue;
    }
}