<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\Registry;
use LaravelCMF\Base\Models\Eloquent\BaseNode;

class ParentNode extends ManyToOne
{
    /** @var BaseNode $parentNode */
    protected $parentNode;

    public function processFieldValue($fieldValue)
    {
        $fieldValue = $this->getResource()->instance()->getBaseAttribute('parent_id');

        if($fieldValue) {
            $this->getRelatedResource()->loadResourceModel($fieldValue);
        }

        return $fieldValue;
    }


    public function processSettings($settings)
    {
        if(!isset($settings['relation']) || !class_exists($settings['relation'])) {
            //assume the calling class
            $settings['relation'] = $this->getResource()->getResourceCmfProperty('baseClass');
        }
        return parent::processSettings($settings);
    }

    public function processRequest(Request $request)
    {
        //we have the ID!
        if ($request->exists($this->getRequestKey())) {
            $processedValue = $request->input($this->getRequestKey(), null);
            $baseClass = $this->getSetting('relation');
            $baseResource = Registry::instance()->getResourceModelByClass($baseClass);
            if($resourceModel = $baseResource->loadResourceModel($processedValue)) {
                //now use NodeTrait method to attach it as a Parent.
                $this->setFieldValue($processedValue);
                $this->parentNode = $resourceModel;
            }
        }
    }

    public function updateField()
    {

        if ($this->dirty() &&
            (!is_null($this->fieldValue) && !empty($this->parentNode))
        ) {
            /** @var BaseNode $resourceModel */
            $resourceModel = $this->getResource()->getResourceModel();
            $resourceModel->attachToParent($this->parentNode);
        }
    }


    public function display()
    {
        return $this->getRelatedResource()->display();
    }

}