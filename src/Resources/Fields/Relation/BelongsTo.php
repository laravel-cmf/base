<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

use Illuminate\Http\Request;

/**
 * The current Resource can inherit the form from a BelongsTo relationship when creating/editing.
 * @package LaravelCMF\Base\Resources\Fields\Relation
 */
class BelongsTo extends AbstractFieldRelation
{
    protected $form_template = null;
    //@todo inherit or embedded?

    public function transform()
    {
        //todo return RelatedModel but ignore the inverse of this relationship?
    }

    public function processRequest(Request $request)
    {
    }


}