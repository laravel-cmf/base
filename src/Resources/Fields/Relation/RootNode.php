<?php

namespace LaravelCMF\Base\Resources\Fields\Relation;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\BaseResource;
use LaravelCMF\Base\Resources\Registry;

class RootNode extends BelongsTo
{
    /**
     * @return BaseResource
     */
    public function getRelatedResource()
    {
        if(empty($this->relatedResource)) {
            $parentResource        = $this->getSetting('inherit') ? null : $this->getResource();
            $this->relatedResource = Registry::instance()->createResource($this->getModelClass(), $parentResource);
        }
        return $this->relatedResource;
    }

    public function processSettings($settings)
    {
        $rootNode = $this->getResource()->getResourceCmfProperty('rootNode');

        //We must have a root node which implements the correct NodeTrait
        if(!$rootNode || !class_exists($rootNode) || !class_implements($rootNode, Kalnoy\Nestedset\NodeTrait::class)){
            throw new \Exception('The root node did not exist or was not correctly configured.');
        }

        $settings['relation'] = $rootNode;

        $settings = parent::processSettings($settings);

        return $settings;
    }

    public function processRequest(Request $request)
    {
        //handle the request data for our related model here.

    }

}