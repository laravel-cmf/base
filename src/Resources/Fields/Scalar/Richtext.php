<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use LaravelCMF\Base\Resources\Fields\Field;

class Richtext extends Field
{
    protected $form_template = 'admin.fields.form.richtext';

}