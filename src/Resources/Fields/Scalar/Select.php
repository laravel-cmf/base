<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use LaravelCMF\Base\Resources\Fields\Field;

class Select extends Field
{
    protected $form_template = 'admin.fields.form.select';

    public function getOptions()
    {
        $optionsSetting = $this->getSetting('options', []);
        $options = [];
        foreach($optionsSetting as $value => $data) {
            $options[] = [
                'value' => $value,
                'label' => $data,
                'selected' => $this->getFieldValue() === $value,
            ];
        }
        return $options;
    }
}