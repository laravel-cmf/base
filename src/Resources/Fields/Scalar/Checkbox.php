<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\Fields\Field;

class Checkbox extends Field
{
    protected $form_template = 'admin.fields.form.checkbox';

    public function processRequest(Request $request)
    {
        $processedData       = $request->input($this->getRequestKey(), null);
        $this->setFieldValue($processedData ? 1 : 0);
    }

    /**
     * Return the rendered value of this resource field to display in list views.
     * @return mixed
     */
    public function display()
    {
        return $this->value() ? 'Yes' : 'No';
    }

}