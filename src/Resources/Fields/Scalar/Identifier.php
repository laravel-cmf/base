<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use LaravelCMF\Base\Resources\Fields\Field;

class Identifier extends Field
{

    protected $form_template = 'admin.fields.form.identifier';


}