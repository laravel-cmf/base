<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use LaravelCMF\Base\Resources\Fields\Field;

class Text extends Field
{
    protected $form_template = 'admin.fields.form.text';
}