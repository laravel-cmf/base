<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use LaravelCMF\Base\Resources\Fields\Field;

class Radio extends Field
{
    protected $form_template = 'admin.fields.form.radio';

    /**
     * Return the rendered value of this resource field to display in list views.
     * @return mixed
     */
    public function display()
    {
        $options = $this->getSetting('options', false);

        return $options && isset($options[$this->getValue()]) ? $options[$this->getValue()] : parent::displayList();

    }
}