<?php

namespace LaravelCMF\Base\Resources\Fields\Scalar;

use Illuminate\Http\Request;
use LaravelCMF\Base\Resources\Fields\Field;

class Password extends Field
{
    protected $form_template = 'admin.fields.form.password';
    protected $passwordConfirm;
    protected $password;

    public function processRequest(Request $request)
    {
        $passwordData       = $request->input($this->getRequestKey(), null);

        if($passwordData && !empty($passwordData['value']) && !empty($passwordData['confirm'])) {
            $this->password = $passwordData['value'];
            $this->passwordConfirm = $passwordData['confirm'];
            if($this->password !== $this->passwordConfirm) {
                $this->addError('The passwords do not match.');
            } else {
                $this->setFieldValue($this->encrypt($passwordData['value']));
            }
        }
    }

    public function encrypt($value)
    {
        return bcrypt($value);
    }
}