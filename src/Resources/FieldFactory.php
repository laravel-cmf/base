<?php

namespace LaravelCMF\Base\Resources;

use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;
use LaravelCMF\Base\Resources\Fields\Checkbox;
use LaravelCMF\Base\Resources\Fields\Field;
use LaravelCMF\Base\Resources\Fields\Identifier;
use LaravelCMF\Base\Resources\Fields\Image;
use LaravelCMF\Base\Resources\Fields\Password;
use LaravelCMF\Base\Resources\Fields\Radio;
use LaravelCMF\Base\Resources\Fields\Relation\AbstractFieldRelation;
use LaravelCMF\Base\Resources\Fields\Relation\ManyToMany;
use LaravelCMF\Base\Resources\Fields\Relation\ManyToOne;
use LaravelCMF\Base\Resources\Fields\Richtext;
use LaravelCMF\Base\Resources\Fields\Select;
use LaravelCMF\Base\Resources\Fields\Text;
use LaravelCMF\Base\Resources\Fields\Textarea;

class FieldFactory
{
    protected $defaultField = 'text';

    var $fieldClasses = [];

    /**
     * FieldFactory constructor.
     */
    public function __construct()
    {
        $this->fieldClasses = CMF::configGet('resource-fields');
    }

    public function createResourceFields(BaseResource $baseResource)
    {
        $resourceFields = [];

        $fields               = $baseResource->fields();

        //We may have Fields that require additional infomation.
        //Perhaps we have a SLUGGABLE field (URL?) and we need to run a custom CREATE
        $position = 1;

        foreach ($fields as $fieldKey => $fieldOptions) {

            $fieldClass = $this->getResourceFieldClass($fieldOptions);

            //Get raw value
            $fieldValue = $baseResource->hasModel() ? $baseResource->getResourceModel()->getProperty($fieldKey) : null;

            $resourceField = $fieldClass::create($baseResource, $fieldKey, $fieldValue, $fieldOptions);
            //Check if we are inheriting this field from a related model (eg. Node types).

            if($this->shouldInheritFieldsFrom($resourceField)) {
                //Get the fields we are inheriting
                /** @var AbstractFieldRelation $resourceField */
                $fields = $resourceField->getRelatedResource()->getResourceFields();
                dump($fields);
                foreach($fields as $relatedField) {
                    $relatedField->setFieldPosition($position);
                    //Add the resource field to the array we will return to the base resource asking for this.
                    $resourceFields[$relatedField->getFieldKey()] = $relatedField;
                    $position++;
                }
                die(1);
            } else {
                $resourceField->setFieldPosition($position);
                //Add the resource field to the array we will return to the base resource asking for this.
                $resourceFields[$fieldKey] = $resourceField;
                $position++;
            }

        }

//        $resourceFields = $this->sortFields($resourceFields);
        return $resourceFields;
    }

//    public function build(CMFAdminResource $resource)
//    {
//        if(!$resource->hasModel()) {
//            throw new \Exception('The Resource must have a model.');
//        }
//
//        $groupedFormFields = $this->getGroupedFormFields($resource);
//
//        return $groupedFormFields;
//    }
    /**
     * @param array $fieldOptions
     * @return Field
     * @throws \Exception
     */
    public function getResourceFieldClass(array $fieldOptions = [])
    {
        $fieldClass = null;
        $fieldType = !empty($fieldOptions['field']) ? $fieldOptions['field'] : $this->defaultField;

        if(isset($this->fieldClasses[$fieldType]) ) {
            $fieldClass = $this->fieldClasses[$fieldType];
        } else if(in_array($fieldType, $this->fieldClasses)) {
            $fieldClass = $fieldType;
        } else {
            throw new \Exception('The field does not exist '.$fieldType);
        }
        return $fieldClass;
    }

    protected function shouldInheritFieldsFrom(CMFResourceField $resourceField)
    {
        return $resourceField->getType() === Field::TYPE_RELATION && $resourceField->getSetting('inherit');
    }
}