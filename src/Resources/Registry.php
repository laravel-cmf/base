<?php
//store model keys -> class names here to allow referencing of models that exist within our CMF

namespace LaravelCMF\Base\Resources;

use LaravelCMF\Base\CMF;
use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Models\Eloquent\BaseNode;

class Registry
{

    /**
     * @var Registry
     */
    public static $instance;

    /**
     * @var ResourceModel[]
     */
    protected $resources = [];

    /**
     * @var ResourceModel[]
     */
    protected $descendants = [];

    /**
     * Registry constructor.
     */
    public function __construct()
    {
        $this->initModelRegistry();
        $this->getDescendants();
        static::$instance = $this;
    }

    /**
     * Reuse the same instance across static calls.
     * @return Registry
     */
    public static function instance()
    {
        if (is_null(static::$instance)) {
            //Using service container connect the model instance
            app(self::class);
        }
        return static::$instance;
    }

    /*
     * Hydrate known models with all defined models in the config, and any defined models in the sidebar.
     */
    public function initModelRegistry()
    {
        //Get all Models declared in the sidebar config, model config
        $resources = array_unique(array_merge(
            CMF::configGet('models', []),
            array_map(function ($v) {
                return (string)$v['model'];
            }, array_filter(CMF::configGet('cmf.sidebar', []), function ($v) {
                return isset($v['model']) && is_string($v['model']);
            }))
        ));
//        dd($resources);

        foreach ($resources as $key => $resourceModel) {
            if (!in_array(ResourceModel::class, class_implements($resourceModel))) {
                throw new \Exception(sprintf('The model %s is not a proper CMF resource model.', $resourceModel));
            }

            $baseResource = new BaseResource($resourceModel);
            $resourceKey  = $baseResource->getResourceKey();
            if (!$resourceKey || $this->getResourceModelByKey($resourceKey)) {
                throw new \Exception('Could not get model key, or the model key was duplicated: ' . $resourceKey);
            }

            $this->resources[$resourceKey] = $resourceModel;
        }
    }

    /**
     * @param $resourceKeyOrClassName
     * @return bool
     */
    public function resourceExists($resourceKeyOrClassName)
    {
        if (is_object($resourceKeyOrClassName)) {
            $resourceKeyOrClassName = get_class($resourceKeyOrClassName);
        }
        return array_key_exists($resourceKeyOrClassName, $this->resources)
        || in_array($resourceKeyOrClassName, $this->resources);
    }

    /**
     * @param $key
     * @return false|BaseResource
     */
    public function getResourceModelByKey($key)
    {
        $resources = $this->resources;

        if (!isset($resources[$key])) {
            return false;
        }

        return $this->createResource($resources[$key]);

    }

    /**
     * @param $class
     * @return BaseResource
     * @throws \Exception
     */
    public function getResourceModelByClass($class)
    {
        $resources = $this->resources;

        $resource = array_filter($resources, function ($resource) use ($class) {
            return $resource === $class;
        });

        if (count($resource) !== 1) {
            throw new \Exception('Too many resources or no resources were returned for class ' . $class);
        }

        return $this->createResource(current($resource));
    }

    /**
     * @return ResourceModel[]
     */
    public function getResources()
    {
        return $this->resources;
    }

    public function getDescendants(BaseResource $resource = null)
    {
        if (empty($this->descendants)) {
            $descendants = [];
            $resources   = $this->getResources();

            /** @var string $class */
            foreach ($resources as $class) {
                $resourceDescendants = [];
                foreach ($resources as $resourceClass) {
                    if ($resourceClass === $class) {
                        continue;
                    }

                    $baseResource = $this->createResource($resourceClass);
                    $extends      = $baseResource->getResourceCmfProperty('resourceExtends');

                    if (!is_array($extends)) {
                        $extends = [$extends];
                    }
                    if (in_array($class, $extends)) {
                        $resourceDescendants[] = $baseResource;
                    }
                }
                if (!empty($resourceDescendants)) {
                    $descendants[$class] = $resourceDescendants;
                }
            }
            $this->descendants = $descendants;
        }

        if ($resource) {
            $class = $resource->getClassName();
            return isset($this->descendants[$class]) ? $this->descendants[$class] : null;
        }

        return $this->descendants;
    }

    public function createStaticResources()
    {
        foreach ($this->getResources() as $resource) {
            $baseResource = $this->createResource($resource);
            //check if it's static and if so, make sure one exists.
            //todo make this a global thing somewhere
            if ($baseResource->getResourceCmfProperty('static')) {
                $baseResource->loadResourceModel();
            }
        }
        return $this;
    }

    /**
     * @param ResourceModel|string $resource
     * @param BaseResource|null $parentResource
     * @return BaseResource
     * @throws \Exception
     */
    public function createResource($resourceModel, BaseResource $parentResource = null)
    {
        if (!$this->resourceExists($resourceModel)) {
            throw new \Exception('Cannot create a CMF Resource with that resource model.');
        }

        return app(BaseResource::class, [$resourceModel, $parentResource]);
    }

    /**
     * todo abstract this to a layer that knows about the db implementation
     */
    public function createListeners()
    {
        $superClasses = $this->getDescendants();

        foreach ($superClasses as $superClass => $descendants) {
            foreach ($descendants as $baseResource) {
                /** @var BaseNode $class */
                $class = $baseResource->getClassName();
                $class::created(function ($childNode) {
                    $baseNode = $childNode->getBaseNode();
                    $baseNode->node()->associate($childNode);
                });
                $class::saved(function ($childNode) {
                    $baseNode = $childNode->getBaseNode();
                    $baseNode->save();
                });
            };
        }
        return $this;
    }
}