<?php

namespace LaravelCMF\Base\Resources;

class Repository
{
    //use this to find resources!

    /**
     * @param Resource $resource
     * @return Resource[]
     */
    public function getIndex($resource)
    {
        if (is_a($resource, BaseResource::class)) {
            //for now it is only Eloquent..
            $class = $resource->getClassName();
        } else {
            $class = $resource;
        }
        $data = $class::all();
        return array_map(function ($result) {
            $baseResource = Registry::instance()->createResource($result);
            return $baseResource->superclass() && $result->node ? Registry::instance()->createResource($result->node) : $baseResource;
        }, $data->all());
    }

    public function getItem($resource, $resourceId)
    {
        if (is_a($resource, BaseResource::class)) {
            //for now it is only Eloquent..
            $class = $resource->getClassName();
        } else {
            $class    = $resource;
            $resource = $this->getResource($class);
        }

        $data = $this->findResourceModelById($class, $resource->identifier(), $resourceId);
        if ($data) {
            return $resource->setResourceModel($data);
        }
    }

    public function getItems($resource, $resourceIds)
    {
        if (is_a($resource, BaseResource::class)) {
            //for now it is only Eloquent..
            $class = $resource->getClassName();
        } else {
            $class    = $resource;
            $resource = $this->getResource($class);
        }

        $results = $class::whereIn($resource->identifier(), $resourceIds)->get();

        if ($results) {
            return array_map(function ($result) {
                $baseResource = Registry::instance()->createResource($result);
                return $baseResource->superclass() && $result->node ? Registry::instance()->createResource($result->node) : $baseResource;
            }, $results->all());
        }

    }

    public function saveItem(BaseResource $adminResource)
    {
        $resourceModel = $adminResource->getResourceModel();
        $resourceModel->save();
    }

    public function deleteItem(BaseResource $adminResource)
    {
        $resourceModel = $adminResource->getResourceModel();
        //todo need to call something on each field to ensure deletion properly
        $adminResource->beforeDelete();
        $resourceModel->delete();
    }

    public function findResourceModelById($resourceClass, $identifierField, $id)
    {
        return $resourceClass::where($identifierField, '=', $id)->first();
    }

    public function findAll($model)
    {
        if ($adminResource = Registry::instance()->getResourceModelByClass($model)) {
            //for now it is only Eloquent..
            $class = $adminResource->getClassName();
            $data  = $class::all();
            return array_map(function ($result) {
                return new BaseResource($result);
            }, $data->all());
        }
        return null;
    }

    public function findStaticResourceModel($resourceClass)
    {
        return $resourceClass::first();
    }

    protected function getResource($class)
    {
        return Registry::instance()->getResourceModelByClass($class);
    }
}