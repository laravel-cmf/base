<?php

namespace LaravelCMF\Base\Resources;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use LaravelCMF\Base\Display\Form\Form;
use LaravelCMF\Base\Models\Contracts\ResourceModel;
use LaravelCMF\Base\Models\Eloquent\Traits\IsRootNode;
use LaravelCMF\Base\Resources\Contracts\CMFResourceField;
use LaravelCMF\Base\Resources\Fields\Field;
use LaravelCMF\Base\Resources\Contracts\CMFResource;
use LaravelCMF\Base\Resources\Fields\FieldResource;
use LaravelCMF\Base\Resources\Fields\Relation\BelongsTo;

class BaseResource implements CMFResource
{

    protected $allowed_actions = [
        'create' => true,
        'edit' => true,
        'delete' => true,
        'view' => false
    ];

    /** @var  string $className */
    protected $className;

    /**
     * The Unique key we use to refer to this model in the model registry.
     * @var  string $resourceKey
     */
    protected $resourceKey;

    /** @var  ResourceModel $resourceModel */
    protected $resourceModel;

    /** @var  Field[] */
    protected $resourceFields;

    /**
     * @var BaseResource|null
     */
    protected $parentResource;

    /**
     * Pass in Resource = either a Classname or a ResourceModel that the CMF is taking care of
     * Parent Resource if this is a related model eg. in a nested form
     * @param string|object $resource
     * @param BaseResource $parentResource
     */
    public function __construct($resource, BaseResource $parentResource = null)
    {
        if (is_string($resource) && class_exists($resource)) {
            $this->className = $resource;
        } else {
            if (is_object($resource) && class_implements($resource, ResourceModel::class)) {
                $this->resourceModel = $resource;
                $this->className     = get_class($resource);
            }
        }


        $this->resourceKey = $this->getResourceCmfProperty('resourceKey',
            Str::snake(Str::plural(basename(str_replace('\\', '/', $this->getClassName()))), '-'));

        if ($parentResource) {
            $this->parentResource = $parentResource;
        }

        $this->allowed_actions = $this->allowed_actions() + $this->allowed_actions;
        if ($this->getResourceCmfProperty('static')) {
            $this->allowed_actions['create'] = false;
            $this->allowed_actions['delete'] = false;
        }
//        dump($this->getClassName(), $this->allowed_actions);
    }

    /**
     * Get current Model instance or create a new one.
     * @return ResourceModel
     */
    public function instance()
    {
        if (!$this->hasModel()) {
            $this->resourceModel = $this->blank();
        }
        return $this->resourceModel;
    }

    /**
     * Create a new instance of the class.
     */
    public function blank()
    {
        //todo have a model factory take care of this.
        $resourceModel = app($this->getClassName());
        return $resourceModel;
    }

    /**
     * Try to find in the repository the resource model and populate it here.
     * @param $identifier
     * @return mixed
     */
    public function loadResourceModel($identifier = null)
    {
        /** @var Repository $repository */
        $repository = app(Repository::class);
        if ($identifier && $resourceModel = $repository->findResourceModelById($this->getClassName(),
                $this->identifier(), $identifier)
        ) {
            $this->setResourceModel($resourceModel);
        } else {
            if (empty($identifier) && $this->getResourceCmfProperty('static')
                && !$this->hasModel()
            ) {
                $resourceModel = $repository->findStaticResourceModel($this->getClassName());

                if (!$resourceModel) {
                    $this->instance();
                    $this->resourceModel->base();
                    //Set default values
                    foreach($this->getResourceFields() as $resourceField) {
                        if($resourceField->getFieldKey() == 'title') {
                            $resourceField->setFieldValue($resourceField->getSetting('default', $this->singular()));
                        }
                        $resourceField->updateField();
                    }
                    $repository->saveItem($this);
                }
            }
        }

        return $this->resourceModel;
    }

    public function saveResourceModel()
    {
        if ($this->hasModel()) {
            /** @var Repository $repository */
            $repository = app(Repository::class);
            $repository->saveItem($this);
        }
    }

    public function create(Request $request)
    {
        $this->instance();
        return $this->update($request);
    }

    public function update(Request $request)
    {
        return $this->updateFields($request);
    }

    public function updateFields(Request $request)
    {
        /** @var Field[] $presentFields */
        $presentFields = array_filter($this->getResourceFields(),
            function (Field $resourceField) use ($request) {
                //Only process fields in the request and our settings field if we have it
                return $request->exists($resourceField->getRequestKey()) || $resourceField->getFieldKey() === 'settings';
            });

        //verify the data for those fields is valid
        $validData = true;
        /** @var Field $resourceField */
        foreach ($presentFields as $resourceField) {
            $resourceField->processRequest($request);
            if (!$resourceField->valid()) {
                $validData = false;
                break;
            }
        }

        //if we have valid data then update the fields with the new data
        if ($validData) {
            //proceed to complete the update
            foreach ($presentFields as $resourceField) {
                $resourceField->updateField();
            }
        }

        return $validData;
        //loop processed data and update those fields
    }

    public function beforeDelete()
    {
        if (!$this->hasModel()) {
            return;
        }
        foreach ($this->getResourceFields() as $resourceField) {
            if (method_exists($resourceField, 'delete')) {
                $resourceField->delete();
            }
        }
    }

    /**
     * @return bool
     */
    public function hasModel()
    {
        return !!$this->getResourceModel();
    }

    public function modelExists()
    {
        return !empty($this->getResourceIdentifier());
    }

    /**
     * Get the field identifier, defaults to 'id'
     * @return mixed
     */
    public function identifier()
    {
        return $this->getResourceCmfProperty('identifier', 'id');
    }

    public function menu_title()
    {
        return $this->getResourceCmfProperty('menuTitle', ucfirst($this->getResourceKey()));
    }

    public function slug_fields()
    {
        return $this->getResourceCmfProperty('slugFields', []);
    }

    /**
     * The field to display in lists etc
     * @return mixed
     */
    public function display_field()
    {
        return $this->getResourceCmfProperty('displayField', $this->identifier());
    }

    /**
     * Fields to be shown in list views
     * @return array
     */
    public function list_fields()
    {
        return $this->getResourceCmfProperty('listFields', [$this->identifier()]);
    }

    /**
     * Key => Settings array of fields the CMF will manage/use.
     * @return array
     */
    public function fields()
    {
        $defaultFields = [];
        //always add the identifier to the fields if we have it
        if ($this->hasModel()) {
            $defaultFields[$this->identifier()] = [
                'field' => 'identifier',
                'hidden' => true
            ];
        }
        $class = $this->getClassName();
        if ($extendedClass = $this->resourceExtends()) {
            $extendedClass = $extendedClass->getClassName();
            $parentClasses = [$extendedClass => $extendedClass] + class_parents($extendedClass);
        } else {
            $parentClasses = class_parents($class);
        }
        $fields = $this->getResourceCmfProperty('fields', [], $class);
        if (!empty($parentClasses)) {
            foreach ($parentClasses as $parentClass) {
                $parentClassFields = $this->getResourceCmfProperty('fields', [], $parentClass);
                if (!empty($parentClassFields)) {
                    $fields = array_merge($parentClassFields, $fields);
                }
            }
        }

        return $defaultFields + $fields;
    }

    public function groups()
    {
        return $this->getResourceCmfProperty('groups', []);
    }

    public function tabs()
    {
        //If we have a 'nested' or 'inherit' resource then we should
        //combine the tabs from this and the rootNode.

        return $this->getResourceCmfProperty('tabs', []);
    }

    public function singular()
    {
        return $this->getResourceCmfProperty('singular', Str::singular($this->getResourceKey()));
    }

    public function plural()
    {
        return $this->getResourceCmfProperty('plural', $this->getResourceKey());
    }

    public function superclass()
    {
        return in_array(IsRootNode::class, class_uses($this->getClassName()));
    }

    public function resourceExtends () {
        if ($extendedResource = $this->getResourceCmfProperty('resourceExtends')) {
            return Registry::instance()->getResourceModelByClass($extendedResource);
        }
        return null;
    }

    public function allowed_actions()
    {
        return $this->getResourceCmfProperty('actions', []);
    }

    public function list_actions()
    {
        return $this->getResourceCmfProperty('listActions', []);
    }

    /**
     * Returns the value of the property which is the identifier for this model.
     * Property defaults to 'id', overridden by static variable _model_idenftifier.
     * @return string
     */
    public function getResourceIdentifier()
    {
        if (!$this->hasModel()) {
            return null;
        }

        $identifier = $this->identifier();
        return $this->getResourceModel()->getProperty($identifier);
    }

    public function icon()
    {
        return $this->getResourceCmfProperty('icon', 'fa-file-o');
    }

    public function display()
    {
        return $this->hasModel() ? strval($this->getResourceModel()->getProperty($this->display_field())) : null;
    }

    public function template()
    {
        return $this->getResourceCmfProperty('template', false);
    }

    /**
     * Determine if an action is allowed by this user/with this model
     * @param $action
     * @return bool|mixed
     */
    public function can($action)
    {
        return array_key_exists($action, $this->allowed_actions) ? $this->allowed_actions[$action] : false;
    }

    public function getDescendants($action = null)
    {
        $descendants = Registry::instance()->getDescendants($this);
        //return an array of base resources
        return !empty($descendants) ? array_filter($descendants,
            function (BaseResource $descendant) use ($action) {
            return $action ? $descendant->can($action) : $descendant;
        }) : false;
    }

    /**
     * @return bool
     */
    public function resourceModelExists()
    {
        return !empty($this->getResourceIdentifier());
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @return string
     */
    public function getResourceKey()
    {
        return $this->resourceKey;
    }

    /**
     * The Scoped Resource Key using dot notation to illustrate hierarchy, eg. grand-parent-resource.parent-resource
     * Recursively looks up parents to return the full path.
     * @param null $childScopedResourceKey
     * @return string
     * @internal param null $childResourcePath
     */
    public function getScopedResourceKey($childScopedResourceKey = null)
    {
        $fullResourceKey = $this->getResourceKey() . ($childScopedResourceKey ? '.' . $childScopedResourceKey : '');
        if ($parentResource = $this->getParentResource()) {
            return $parentResource->getScopedResourceKey($fullResourceKey);
        }
        return $fullResourceKey;
    }

    /**
     * @return BaseResource|null
     */
    public function getParentResource()
    {
        return $this->parentResource;
    }

    /**
     * @param BaseResource $parentResource
     * @return BaseResource
     */
    public function setParentResource($parentResource)
    {
        $this->parentResource = $parentResource;
        return $this;
    }

    /**
     * @return ResourceModel
     */
    public function getResourceModel()
    {
        return $this->resourceModel;
    }

    /**
     * @param ResourceModel $resourceModel
     * @return BaseResource
     */
    public function setResourceModel(ResourceModel $resourceModel)
    {
        $this->resourceModel = $resourceModel;
        return $this;
    }

    /**
     * @param array $fieldKeys
     * @param bool $regen
     * @return Fields\Field[]
     */
    public function getResourceFields($fieldKeys = [], $regen = false)
    {
        if (!$this->resourceFields || $regen) {
            $fieldFactory         = new FieldFactory();
            $this->resourceFields = $fieldFactory->createResourceFields($this);
        }

        if (!empty($fieldKeys)) {
            if (is_string($fieldKeys)) {
                $fieldKeys = [$fieldKeys];
            }
            return array_filter($this->resourceFields, function (Field $field) use ($fieldKeys) {
                return in_array($field->getFieldKey(), $fieldKeys);
            });
        }

        return $this->resourceFields;
    }

    public function getResourceField($fieldKey)
    {
        return !empty($this->getResourceFields([$fieldKey])) ? $this->getResourceFields([$fieldKey])[0] : null;
    }

    public function setResourceFieldSetting($key, $value)
    {
        $settings = isset($this->resourceFields['settings']) ? $this->resourceFields['settings'] : false;
        if ($settings) {
            $settingsValue = $settings->value();
            Arr::set($settingsValue, $key, $value);
            $settings->setFieldValue($settingsValue);
        }
    }

    /**
     * Return the resource fields for only the specified list fields on this admin resource.
     * @return array
     */
    public function getListFields()
    {
        $listFields         = $this->list_fields();
        $listResourceFields = $this->getResourceFields(); // array_filter(, function(CMFResourceField $resourceField) use($listFields) {
        //});

        $listFields = array_map(function ($fieldKey) use ($listResourceFields) {

            if (strpos($fieldKey, '.')) {
                $relatedKey = substr($fieldKey, 0, strpos($fieldKey, '.'));
                $fieldKey   = substr($fieldKey, strpos($fieldKey, '.') + 1);
                if (isset($resourceFields[$relatedKey])
                    && method_exists('getRelatedResource', $resourceFields[$relatedKey])
                ) {
                    //check if we have the rest of the resource in there.
                    /** @var BaseResource $relatedResource */
                    $relatedResource = $resourceFields[$relatedKey]->getBaseResource();
                    return $relatedResource->getResourceField($fieldKey);
                }
                return null;
            } else {
                return !empty($listResourceFields[$fieldKey]) ? $listResourceFields[$fieldKey] : null;
            }
        }, array_combine($listFields, $listFields));

        return $listFields;
    }

    public function getResourceFieldErrors()
    {
        return array_map(function (Field $resourceField) {
            return $resourceField->getErrors();
        }, array_filter($this->getResourceFields(), function (Field $resourceField) {
            return !empty($resourceField->getErrors());
        }));
    }

    public function setResourceFieldErrors($errors)
    {
        $resourceFieldsWithErrors = array_filter($this->getResourceFields(),
            function (Field $resourceField) use ($errors) {
                return !empty($errors[$resourceField->getFieldKey()]);
            });
        /** @var Field $resourceFieldsWithError */
        foreach ($resourceFieldsWithErrors as $resourceFieldsWithError) {
            $resourceFieldsWithError->setErrors($errors[$resourceFieldsWithError->getFieldKey()]);
        }
    }

    public function getIndexLink()
    {
        return cmf_url($this->plural());
    }

    public function getViewLink()
    {
        return cmf_url($this->plural() . '/' . $this->getResourceIdentifier());
    }

    public function getCreateLink()
    {
        return cmf_url($this->plural() . '/create');
    }

    public function getEditLink()
    {
        return cmf_url($this->plural() . '/' . $this->getResourceIdentifier() . '/edit');
    }

    public function getDeleteLink()
    {
        return cmf_url($this->plural() . '/' . $this->getResourceIdentifier() . '/delete');
    }

    public function getActionLink($action)
    {
        $link    = '';
        $actions = $this->list_actions();
        if (isset($actions[$action])) {
            $link = cmf_url($this->plural() . '/' . $this->getResourceIdentifier() . '/action?key=' . $action);
        }
        return $link;
    }

    public function getForm()
    {
        //todo check if we have any belongsTo relationship
        //if the 'inherit' value is true (by default) then we will render that form instead.
        $belongsTo = array_filter($this->getResourceFields(), function (CMFResourceField $resourceField) {
            return in_array(BelongsTo::class, class_parents($resourceField)) && $resourceField->getSetting('inherit',
                true);
        });
        $config    = [
            'tabs' => $this->tabs(),
            'groups' => $this->groups(),
            'fields' => $this->getResourceFields(),
        ];

        if (!empty($belongsTo)) {
            /** @var BaseResource $parentResource */
            $parentResource = current($belongsTo)->getBaseResource();

            //we can only merge tabs and groups. fields will be the responsibility of the belongsTo field to handle.
            $config = [
                'tabs' => array_merge($parentResource->tabs(), $config['tabs']),
                'groups' => array_merge($parentResource->groups(), $config['groups']),
            ];
        }

        return Form::create($this->getScopedResourceKey(), $config);
    }

    public function getResourceCmfProperty($property, $default = null, $className = null)
    {
        $value     = $default;
        $className = $className ? $className : $this->getClassName();

        if (property_exists($className, '_' . $property)) {
            $property = '_' . $property;
            $value    = $className::$$property;
        } else {
            if (method_exists($className, $property)) {
                $value = call_user_func([$className, $property]);
            }
        }
        return $value;
    }

    public function getModelArray()
    {
        $model = [];

        foreach ($this->getResourceFields() as $resourceField) {
            $value = $resourceField->transform();
            //Some fields want to include themselves in the root level.
            $model[$resourceField->getFieldKey()] = $value;
        }
        return $model;
    }

    public function transform()
    {
        return $this->getModelArray();
    }

    public function getJsonData()
    {
        $model = $this->transform();

        return json_encode(['model' => $model]);
    }
}